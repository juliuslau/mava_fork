package sgo.mobile.mava.app.dialogs;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.securepreferences.SecurePreferences;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import sgo.mobile.mava.R;
import sgo.mobile.mava.app.adapter.SpinnerBankAdapter;
import sgo.mobile.mava.app.adapter.SpinnerProductAdapter;
import sgo.mobile.mava.app.beans.BankKMKBean;
import sgo.mobile.mava.app.beans.SpinnerBankBean;
import sgo.mobile.mava.app.beans.SpinnerProductBean;
import sgo.mobile.mava.conf.AppParams;

/**
 * Created by thinkpad on 1/18/2016.
 */
public class DialogRepayment extends android.support.v4.app.DialogFragment {

    View view;
    SecurePreferences sp;

    LinearLayout layout_bank, layout_product, layout_remark_text, layout_remark_number, layout_ok_cancel, layout_remark_text_space;
    Spinner spinnerBank, spinnerProduct;
    TextView alertSpinnerBank, alertSpinnerProduct;
    EditText etRemarkText, etRemarkNumber, etRemarkTextSpace;
    Button btnOK, btnCancel;

    SpinnerBankAdapter bankAdapter;
    SpinnerProductAdapter productAdapter;

    String bankKMK;
    String type, remark, bank_code, product_code;

    //untuk spinner bank
    private ArrayList<SpinnerBankBean> BankList;
    //untuk spinner product
    private ArrayList<SpinnerProductBean> ProductList;
    //simpan data bank kmk
    private ArrayList<BankKMKBean> BankKMKList;

    public interface NoticeDialogListener {
        void onFinishDialog();
//        void onFinishEditDialog(String inputText);
        void onFinishSpinnerBankDialog(String bankCode, String bankName);
        void onFinishSpinnerProductDialog(String productCode, String productName, String product_h2h);
        void onDismiss(boolean dismiss);
    }

    public DialogRepayment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_invoice_dgi, container);
        getDialog().requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);

        sp = new SecurePreferences(getActivity());
        bankKMK = sp.getString(AppParams.BANK_KMK,"");

        BankList = new ArrayList<>();
        ProductList = new ArrayList<>();
        BankKMKList = new ArrayList<>();

        layout_bank = (LinearLayout) view.findViewById(R.id.layout_bank);
        layout_product = (LinearLayout) view.findViewById(R.id.layout_product);
        layout_remark_text = (LinearLayout) view.findViewById(R.id.layout_remark_text);
        layout_remark_number = (LinearLayout) view.findViewById(R.id.layout_remark_number);
        layout_ok_cancel = (LinearLayout) view.findViewById(R.id.layout_ok_cancel);
        layout_remark_text_space = (LinearLayout) view.findViewById(R.id.layout_remark_text_space);

        spinnerBank = (Spinner) view.findViewById(R.id.spinner_bank);
        spinnerProduct = (Spinner) view.findViewById(R.id.spinner_product);
        alertSpinnerBank = (TextView) view.findViewById(R.id.alertSpinnerBank);
        alertSpinnerProduct = (TextView) view.findViewById(R.id.alertSpinnerProduct);
        etRemarkText = (EditText) view.findViewById(R.id.remark_value_text);
        etRemarkNumber = (EditText) view.findViewById(R.id.remark_value_number);
        etRemarkTextSpace = (EditText) view.findViewById(R.id.remark_value_text_space);

        btnOK = (Button) view.findViewById(R.id.btn_ok);
        btnCancel = (Button) view.findViewById(R.id.btn_cancel);

        Bundle bundle = getArguments();
        if(bundle != null) {
            String remarkHint = bundle.getString("remark_hint");
            type = bundle.getString("payment_type");
            remark = bundle.getString("payment_remark");

            if(type.equalsIgnoreCase(AppParams.ESPAY)) {
                if(!bankKMK.equals("")) {
                    ProductList.add(new SpinnerProductBean("0", getResources().getString(R.string.tidak_ada_data)));
                    try {
                        JSONArray arrBank = new JSONArray(bankKMK);

                        BankList.add(new SpinnerBankBean("0", getResources().getString(R.string.pilih_bank)));
                        for (int i = 0; i < arrBank.length(); i++) {

                            BankKMKBean bankKMKBean = new BankKMKBean();
                            bankKMKBean.setBank_code(arrBank.getJSONObject(i).getString(AppParams.BANK_CODE));
                            bankKMKBean.setBank_name(arrBank.getJSONObject(i).getString(AppParams.BANK_NAME));
                            bankKMKBean.setProduct_code(arrBank.getJSONObject(i).getString(AppParams.PRODUCT_CODE));
                            bankKMKBean.setProduct_name(arrBank.getJSONObject(i).getString(AppParams.PRODUCT_NAME));
                            bankKMKBean.setTx_fee(arrBank.getJSONObject(i).getString(AppParams.TX_FEE));
                            bankKMKBean.setBuyer_fee(arrBank.getJSONObject(i).getString(AppParams.BUYER_FEE));
                            bankKMKBean.setSeller_fee(arrBank.getJSONObject(i).getString(AppParams.SELLER_FEE));
                            bankKMKBean.setProduct_h2h(arrBank.getJSONObject(i).getString(AppParams.PRODUCT_H2H));
                            BankKMKList.add(bankKMKBean);

                            boolean same = false;
                            if(BankList.size() > 0) {
                                for (int j = 0; j < BankList.size(); j++) {
                                    String bankCode = arrBank.getJSONObject(i).getString(AppParams.BANK_CODE);
                                    if(bankCode.equals(BankList.get(j).getBank_code())) {
                                        same = true;
                                        break;
                                    }
                                }
                            }
                            if(same == false) BankList.add(new SpinnerBankBean(arrBank.getJSONObject(i).getString(AppParams.BANK_CODE), arrBank.getJSONObject(i).getString(AppParams.BANK_NAME)));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else {
                    BankList.add(new SpinnerBankBean("0", getResources().getString(R.string.tidak_ada_data)));
                    ProductList.add(new SpinnerProductBean("0", getResources().getString(R.string.tidak_ada_data)));
                }
            }
//            else if(type.equalsIgnoreCase(AppParams.BG) || type.equalsIgnoreCase(AppParams.TS)) {
//                if (!bankKMK.equals("")) {
//                    try {
//                        JSONArray arrBankData = new JSONArray(bankKMK);
//                        BankList.add(new PaymentTypeBean("0", "Choose Bank"));
//                        for (int i = 0; i < arrBankData.length(); i++) {
//                            BankList.add(new PaymentTypeBean(arrBankData.getJSONObject(i).getString(AppParams.BANK_CODE), arrBankData.getJSONObject(i).getString(AppParams.BANK_NAME)));
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                } else {
//                    BankList.add(new PaymentTypeBean("0", getResources().getString(R.string.tidak_ada_data)));
//                }
//            }

            bankAdapter = new SpinnerBankAdapter(getActivity(), android.R.layout.simple_spinner_item, BankList){
                @Override
                public View getDropDownView(int position, View convertView, ViewGroup parent) {
                    View v = null;

                    // If this is the initial dummy entry, make it hidden
                    if (position == 0) {
                        TextView tv = new TextView(getContext());
                        tv.setHeight(0);
                        tv.setVisibility(View.GONE);
                        v = tv;
                    }
                    else {
                        // Pass convertView as null to prevent reuse of special case views
                        v = super.getDropDownView(position, null, parent);
                    }

                    // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
                    parent.setVerticalScrollBarEnabled(false);
                    return v;
                }
            };
            productAdapter = new SpinnerProductAdapter(getActivity(), android.R.layout.simple_spinner_item, ProductList){
                @Override
                public View getDropDownView(int position, View convertView, ViewGroup parent) {
                    View v = null;

                    // If this is the initial dummy entry, make it hidden
                    if (position == 0) {
                        TextView tv = new TextView(getContext());
                        tv.setHeight(0);
                        tv.setVisibility(View.GONE);
                        v = tv;
                    }
                    else {
                        // Pass convertView as null to prevent reuse of special case views
                        v = super.getDropDownView(position, null, parent);
                    }

                    // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
                    parent.setVerticalScrollBarEnabled(false);
                    return v;
                }
            };
            spinnerBank.setAdapter(bankAdapter);
            spinnerProduct.setAdapter(productAdapter);

//            if(type.equalsIgnoreCase(AppParams.CT)) {
//                layout_remark_text_space.setVisibility(View.VISIBLE);
//                layout_ok_cancel.setVisibility(View.VISIBLE);
//
//                etRemarkTextSpace.setHint(remarkHint);
//                etRemarkTextSpace.setText(remark);
//            }
//            else if(type.equalsIgnoreCase(AppParams.BG)) {
//                bank_code = bundle.getString("payment_bank_code");
//                layout_bank.setVisibility(View.VISIBLE);
//                layout_remark_text.setVisibility(View.VISIBLE);
//                layout_ok_cancel.setVisibility(View.VISIBLE);
//
//                for(int i=0 ; i<BankList.size() ; i++) {
//                    if(BankList.get(i).getBank_code().equalsIgnoreCase(bank_code)) {
//                        spinnerBank.setSelection(i);
//                        break;
//                    }
//                }
//
//                etRemarkText.setHint(remarkHint);
//                etRemarkText.setText(remark);
//            }
//            else if(type.equalsIgnoreCase(AppParams.TS)) {
//                bank_code = bundle.getString("payment_bank_code");
//                layout_bank.setVisibility(View.VISIBLE);
//                layout_remark_number.setVisibility(View.VISIBLE);
//                layout_ok_cancel.setVisibility(View.VISIBLE);
//
//                for(int i=0 ; i<BankList.size() ; i++) {
//                    if(BankList.get(i).getBank_code().equalsIgnoreCase(bank_code)) {
//                        spinnerBank.setSelection(i);
//                        break;
//                    }
//                }
//                etRemarkNumber.setHint(remarkHint);
//                etRemarkNumber.setText(remark);
//            }
//            else
            if(type.equalsIgnoreCase(AppParams.ESPAY)) {
                bank_code = bundle.getString("payment_bank_code");
                product_code = bundle.getString("payment_product_code");
                layout_bank.setVisibility(View.VISIBLE);
                layout_product.setVisibility(View.VISIBLE);
                layout_ok_cancel.setVisibility(View.VISIBLE);

                for(int i=0 ; i<BankList.size() ; i++) {
                    if(BankList.get(i).getBank_code().equalsIgnoreCase(bank_code)) {
                        spinnerBank.setSelection(i);
                        break;
                    }
                }

            }

        }


        spinnerBank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position != 0 && type.equalsIgnoreCase(AppParams.ESPAY)) {
                    ProductList.clear();
                    ProductList.add(new SpinnerProductBean("0", getResources().getString(R.string.pilih_product)));

                    String bankCodeChosen = BankList.get(position).getBank_code();
                    Log.d("bankCodeChosen", bankCodeChosen);

                    for(int i = 0 ; i < BankKMKList.size() ; i++) {
                        if(bankCodeChosen.equalsIgnoreCase(BankKMKList.get(i).getBank_code())) {
                            ProductList.add(new SpinnerProductBean(BankKMKList.get(i).getProduct_code(), BankKMKList.get(i).getProduct_name()));
                        }
                    }

                    spinnerProduct.setAdapter(productAdapter);

                    for(int i=0 ; i<ProductList.size() ; i++) {
                        if(ProductList.get(i).getProduct_code().equalsIgnoreCase(product_code)) {
                            spinnerProduct.setSelection(i);
                            break;
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NoticeDialogListener activity = (NoticeDialogListener) getTargetFragment();
                activity.onDismiss(true);
                dismiss();
            }
        });

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (inputValidation()) {
                    NoticeDialogListener activity = (NoticeDialogListener) getTargetFragment();
//                    if (type.equalsIgnoreCase(AppParams.CT)) {
//                        activity.onFinishDialog();
//                        activity.onFinishEditDialog(etRemarkTextSpace.getText().toString());
//                        activity.onFinishSpinnerBankDialog("", "");
//                    } else if (type.equalsIgnoreCase(AppParams.BG) || type.equalsIgnoreCase(AppParams.TS)) {
//                        activity.onFinishDialog();
//                        if (type.equalsIgnoreCase(AppParams.BG))
//                            activity.onFinishEditDialog(etRemarkText.getText().toString());
//                        if (type.equalsIgnoreCase(AppParams.TS))
//                            activity.onFinishEditDialog(etRemarkNumber.getText().toString());
//                        activity.onFinishSpinnerBankDialog(BankList.get(spinnerBank.getSelectedItemPosition()).getPayment_type(),
//                                BankList.get(spinnerBank.getSelectedItemPosition()).getPayment_name());
//                    } else
                if (type.equalsIgnoreCase(AppParams.ESPAY)) {
                        String productCode = ProductList.get(spinnerProduct.getSelectedItemPosition()).getProduct_code();
                        String product_h2h = null;
                        for(int i = 0 ; i < BankKMKList.size() ; i++) {
                            if(productCode.equalsIgnoreCase(BankKMKList.get(i).getProduct_code())) {
                                product_h2h = BankKMKList.get(i).getProduct_h2h();
                            }
                        }
                        activity.onFinishDialog();
                        activity.onFinishSpinnerBankDialog(BankList.get(spinnerBank.getSelectedItemPosition()).getBank_code(),
                                BankList.get(spinnerBank.getSelectedItemPosition()).getBank_name());
                        activity.onFinishSpinnerProductDialog(ProductList.get(spinnerProduct.getSelectedItemPosition()).getProduct_code(),
                                ProductList.get(spinnerProduct.getSelectedItemPosition()).getProduct_name(), product_h2h);
                    }
                    dismiss();
                }
            }
        });


        return view;
    }

    public boolean inputValidation() {

        if(type.equalsIgnoreCase(AppParams.BG) || type.equalsIgnoreCase(AppParams.TS) || type.equalsIgnoreCase(AppParams.ESPAY)) {
            if (BankList.get(spinnerBank.getSelectedItemPosition()).getBank_code().equals("0")) {
                alertSpinnerBank.setVisibility(View.VISIBLE);
                alertSpinnerBank.setText(getString(R.string.bank_validation));
                return false;
            }
            if (!BankList.get(spinnerBank.getSelectedItemPosition()).getBank_code().equals("0")) {
                alertSpinnerBank.setText("");
                alertSpinnerBank.setVisibility(View.GONE);
            }
        }

//        if(type.equalsIgnoreCase(AppParams.BG)) {
//            if (etRemarkText.getText().toString().length() == 0) {
//                etRemarkText.requestFocus();
//                etRemarkText.setError(getString(R.string.remark_validation));
//                return false;
//            }
//        }
//        if(type.equalsIgnoreCase(AppParams.TS)) {
//            if (etRemarkNumber.getText().toString().length() == 0) {
//                etRemarkNumber.requestFocus();
//                etRemarkNumber.setError(getString(R.string.remark_validation));
//                return false;
//            }
//        }

        if(type.equalsIgnoreCase(AppParams.ESPAY)) {
            if (ProductList.get(spinnerProduct.getSelectedItemPosition()).getProduct_code().equals("0")) {
                alertSpinnerProduct.setVisibility(View.VISIBLE);
                alertSpinnerProduct.setText(getString(R.string.product_validation));
                return false;
            }
            if (!ProductList.get(spinnerProduct.getSelectedItemPosition()).getProduct_code().equals("0")) {
                alertSpinnerProduct.setText("");
                alertSpinnerProduct.setVisibility(View.GONE);
            }
        }
        return true;
    }

}
