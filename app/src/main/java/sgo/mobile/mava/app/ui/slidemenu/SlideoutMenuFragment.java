package sgo.mobile.mava.app.ui.slidemenu;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.slidingmenu.lib.SlidingMenu;
import sgo.mobile.mava.AppHelper;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.activities.MainActivity;
import sgo.mobile.mava.app.fragments.*;
import sgo.mobile.mava.conf.AppParams;

public class SlideoutMenuFragment extends SherlockFragment {
    private static SlideoutMenuFragment instance;
    private ActionBar actionBar;
    private MainActivity mActivity = null;
    FragmentManager fm;
    SlidingMenu menu;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static SlideoutMenuFragment getInstance() {
        if (instance == null) {
            return new SlideoutMenuFragment();
        }
        return instance;
    }

    public SlideoutMenuFragment() {
        instance = this;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.menu_layout, container, false);
        final Button menuHome          = (Button) view.findViewById(R.id.menuHome);
        menuHome.setSelected(true);
        final Button menuTopUp         = (Button) view.findViewById(R.id.menuTopUp);
        final Button menuSendEcash     = (Button) view.findViewById(R.id.menuSendEcash);
        final Button menuStore         = (Button) view.findViewById(R.id.menuStore);
        final Button menuPurchase      = (Button) view.findViewById(R.id.menuPurchase);
        final Button menuWithdraw      = (Button) view.findViewById(R.id.menuWithdraw);
        final Button menuSetting       = (Button) view.findViewById(R.id.menuSetting);
        final Button menuSendC2A       = (Button) view.findViewById(R.id.menuSendC2A);
        final Button menuSendC2C            = (Button) view.findViewById(R.id.menuSendC2C);
        final Button menuSendCCL            = (Button) view.findViewById(R.id.menuSendCCL);
        final Button menumenuInvoicePayment = (Button) view.findViewById(R.id.menuInvoicePayment);
        final Button menuDigitalInclusion   = (Button) view.findViewById(R.id.menuDigitalInclusion);
        final Button menuKUM           = (Button) view.findViewById(R.id.menuKUM);

        final Button menuReport             = (Button) view.findViewById(R.id.menuReport);
//        final Button menuRepayment          = (Button) view.findViewById(R.id.menuRepayment);
        final View menuReportDivider        = view.findViewById(R.id.menuReportDivider);
        final View menuRepaymentDivider        = view.findViewById(R.id.menuRepaymentDivider);

        menuHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment newFragment = new MainListFragment();
                switchFragment(newFragment);

                menuHome.setSelected(true);
                menuTopUp.setSelected(false);
                menuSendEcash.setSelected(false);
                menuStore.setSelected(false);
                menuPurchase.setSelected(false);
                menuWithdraw.setSelected(false);
                menuSetting.setSelected(false);
                menuSendC2A.setSelected(false);
                menuSendC2C.setSelected(false);
                menuSendCCL.setSelected(false);
                menumenuInvoicePayment.setSelected(false);
                menuDigitalInclusion.setSelected(false);
                menuReport.setSelected(false);
//                menuRepayment.setSelected(false);
                menuKUM.setSelected(false);
            }
        });

        menuDigitalInclusion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String loginType = AppHelper.getUserType(getActivity());
                if (loginType.equalsIgnoreCase("Member / Agent")) {
                    Fragment newFragment = null;
                    newFragment = new FragmentCommunityInvUser();
                    Bundle bundle = new Bundle();
                    bundle.putString("title", "AR-Collection");
                    newFragment.setArguments(bundle);
                    switchFragment(newFragment);
                }else{
                    Fragment newFragment = null;
                    newFragment = new FragmentCommunityListDGI();
                    switchFragment(newFragment);
                }

//                Fragment newFragment = null;
//                newFragment = new FragmentLCSMenu();
//                switchFragment(newFragment);

                menuDigitalInclusion.setSelected(true);

                menuHome.setSelected(false);
                menuTopUp.setSelected(false);
                menuSendEcash.setSelected(false);
                menuStore.setSelected(false);
                menuPurchase.setSelected(false);
                menuWithdraw.setSelected(false);
                menuSetting.setSelected(false);
                menuSendC2A.setSelected(false);
                menuSendC2C.setSelected(false);
                menuSendCCL.setSelected(false);
                menumenuInvoicePayment.setSelected(false);
                menuReport.setSelected(false);
//                menuRepayment.setSelected(false);
                menuKUM.setSelected(false);
            }
        });

//        menuRepayment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Fragment newFragment = null;
//                newFragment = new FragmentCommunityRepayment();
//                Bundle bundle = new Bundle();
//                bundle.putString("title", getResources().getString(R.string.kmk));
//                newFragment.setArguments(bundle);
//                switchFragment(newFragment);
//
//                menuRepayment.setSelected(true);
//
//                menuHome.setSelected(false);
//                menuTopUp.setSelected(false);
//                menuSendEcash.setSelected(false);
//                menuStore.setSelected(false);
//                menuPurchase.setSelected(false);
//                menuWithdraw.setSelected(false);
//                menuSetting.setSelected(false);
//                menuSendC2A.setSelected(false);
//                menuSendC2C.setSelected(false);
//                menuSendCCL.setSelected(false);
//                menumenuInvoicePayment.setSelected(false);
//                menuDigitalInclusion.setSelected(false);
//                menuReport.setSelected(false);
//                menuKUM.setSelected(false);
//            }
//        });

        menuReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment newFragment = new FragmentCommunityReportDGI();
                switchFragment(newFragment);

                menuReport.setSelected(true);

                menuHome.setSelected(false);
                menuTopUp.setSelected(false);
                menuSendEcash.setSelected(false);
                menuStore.setSelected(false);
                menuPurchase.setSelected(false);
                menuWithdraw.setSelected(false);
                menuSetting.setSelected(false);
                menuSendC2A.setSelected(false);
                menuSendC2C.setSelected(false);
                menuSendCCL.setSelected(false);
                menumenuInvoicePayment.setSelected(false);
                menuDigitalInclusion.setSelected(false);
//                menuRepayment.setSelected(false);
                menuKUM.setSelected(false);
            }
        });

        menuTopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment newFragment = new FragmentTopUpPrincipal();
                switchFragment(newFragment);

                menuTopUp.setSelected(true);
                menuHome.setSelected(false);
                menuSendEcash.setSelected(false);
                menuStore.setSelected(false);
                menuPurchase.setSelected(false);
                menuWithdraw.setSelected(false);
                menuSetting.setSelected(false);
                menuSendC2A.setSelected(false);
                menuSendC2C.setSelected(false);
                menuSendCCL.setSelected(false);
                menumenuInvoicePayment.setSelected(false);
                menuDigitalInclusion.setSelected(false);
                menuReport.setSelected(false);
//                menuRepayment.setSelected(false);
                menuKUM.setSelected(false);
            }
        });

        menuKUM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment newFragment;
                String loginType = AppHelper.getUserType(getActivity());
                if (loginType.equalsIgnoreCase("Member / Agent")) {
                    newFragment = new FragmentListAccKum();
                    switchFragment(newFragment);
                }
                else {
                    newFragment = new FragmentCommunityListDGI();
                    Bundle args = new Bundle();
                    args.putBoolean(AppParams.KUM, true);
                    newFragment.setArguments(args);
                    switchFragment(newFragment);
                }

                menuKUM.setSelected(true);
                menuTopUp.setSelected(false);
                menuHome.setSelected(false);
                menuSendEcash.setSelected(false);
                menuStore.setSelected(false);
                menuPurchase.setSelected(false);
                menuWithdraw.setSelected(false);
                menuSetting.setSelected(false);
                menuSendC2A.setSelected(false);
                menuSendC2C.setSelected(false);
                menuSendCCL.setSelected(false);
                menumenuInvoicePayment.setSelected(false);
                menuDigitalInclusion.setSelected(false);
                menuReport.setSelected(false);
//                menuRepayment.setSelected(false);
            }
        });

        String loginType = AppHelper.getUserType(getActivity());
        if (loginType.equalsIgnoreCase("Collector")) {
            menuReport.setVisibility(View.VISIBLE);
            menuReportDivider.setVisibility(View.VISIBLE);
//            menuRepayment.setVisibility(View.GONE);
            menuRepaymentDivider.setVisibility(View.GONE);
            menuKUM.setVisibility(View.GONE);
        }
        else if (loginType.equalsIgnoreCase("Member / Agent")) {
//            menuRepayment.setVisibility(View.VISIBLE);
            menuRepaymentDivider.setVisibility(View.VISIBLE);
            menuReport.setVisibility(View.GONE);
            menuReportDivider.setVisibility(View.GONE);
            menuKUM.setVisibility(View. GONE);
        }

        return view;
    }

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);

        getSherlockActivity().getActionBar().removeAllTabs();
        getSherlockActivity().getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
    }

}
