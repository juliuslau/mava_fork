package sgo.mobile.mava.app.beans;

/**
 * Created by thinkpad on 7/9/2015.
 */
public class ReportCommunityBean {
    private String comm_id;
    private String comm_code;
    private String comm_name;
    private String sales_alias;
    private String ccy_id;
    private String amount;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCcy_id() {
        return ccy_id;
    }

    public void setCcy_id(String ccy_id) {
        this.ccy_id = ccy_id;
    }

    public String getComm_code() {
        return comm_code;
    }

    public void setComm_code(String comm_code) {
        this.comm_code = comm_code;
    }

    public String getComm_id() {
        return comm_id;
    }

    public void setComm_id(String comm_id) {
        this.comm_id = comm_id;
    }

    public String getComm_name() {
        return comm_name;
    }

    public void setComm_name(String comm_name) {
        this.comm_name = comm_name;
    }

    public String getSales_alias() {
        return sales_alias;
    }

    public void setSales_alias(String sales_alias) {
        this.sales_alias = sales_alias;
    }
}
