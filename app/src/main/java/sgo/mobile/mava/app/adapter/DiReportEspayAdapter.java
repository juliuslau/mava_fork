package sgo.mobile.mava.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import sgo.mobile.mava.R;
import sgo.mobile.mava.app.beans.ReportListEspayModel;
import sgo.mobile.mava.frameworks.math.FormatCurrency;

/**
 * Created by thinkpad on 2/20/2017.
 */

public class DiReportEspayAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<ReportListEspayModel> arrReport;

    public DiReportEspayAdapter(Activity act, ArrayList<ReportListEspayModel> arrReportComm) {
        this.activity = act;
        this.arrReport = arrReportComm;
    }
    public int getCount() {
        // TODO Auto-generated method stub
        return arrReport.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_comm_dgi_report_espay_item, null);
            holder = new ViewHolder();

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        Double dbl_amount = Double.parseDouble(arrReport.get(position).getAmount());
        int int_amount = dbl_amount.intValue();

        holder.tvDatetime    = (TextView) convertView.findViewById(R.id.text_tgl_trans);
        holder.tvStatus = (TextView) convertView.findViewById(R.id.text_tx_status);
        holder.tvMemberName = (TextView) convertView.findViewById(R.id.text_member_name);
//        holder.tvCCy = (TextView) convertView.findViewById(R.id.text_ccyID);
        holder.tvAmount = (TextView) convertView.findViewById(R.id.text_amount);
        holder.tvRemark = (TextView) convertView.findViewById(R.id.text_remark);
        holder.tvProductName = (TextView) convertView.findViewById(R.id.text_product_name);

        holder.tvDatetime.setText(arrReport.get(position).getDatetime());
        String tx_status = arrReport.get(position).getTx_status();
        if(tx_status.equalsIgnoreCase("S")) {
            holder.tvStatus.setText("Berhasil");
        }
        else if(tx_status.equalsIgnoreCase("OR")) {
            holder.tvStatus.setText("Proses");
        }
        else if(tx_status.equalsIgnoreCase("SP")) {
            holder.tvStatus.setText("Suspect");
        }
        else {
            holder.tvStatus.setText("Gagal");
        }
        holder.tvMemberName.setText(arrReport.get(position).getMember_name());
//        holder.tvCCy.setText(arrReport.get(position).getCcy_id());
        holder.tvAmount.setText(FormatCurrency.getRupiahFormat(Integer.toString(int_amount)));
        String remark = arrReport.get(position).getRemark();
        if(!remark.equals("null") && !remark.equals("")) {
            holder.tvRemark.setVisibility(View.VISIBLE);
            holder.tvRemark.setText(remark);
        }
        else
            holder.tvRemark.setVisibility(View.GONE);
        holder.tvProductName.setText(arrReport.get(position).getProduct_name());

        return convertView;
    }

    static class ViewHolder {
        TextView tvDatetime, tvStatus, tvMemberName, tvCCy, tvAmount, tvRemark, tvProductName;
    }
}
