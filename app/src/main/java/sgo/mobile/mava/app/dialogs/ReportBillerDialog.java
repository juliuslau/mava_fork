package sgo.mobile.mava.app.dialogs;/*
  Created by Administrator on 3/6/2015.
 */

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import sgo.mobile.mava.R;
import sgo.mobile.mava.conf.AppParams;
import sgo.mobile.mava.frameworks.math.FormatCurrency;

public class ReportBillerDialog extends DialogFragment implements View.OnClickListener {

    public static final String TAG = "reportBiller Dialog";

    private OnDialogOkCallback callback;
    private Activity mContext;
    private Boolean isActivty = false;


    public interface OnDialogOkCallback {
        void onOkButton();
    }

    public static ReportBillerDialog newInstance(Activity _context, boolean _isActivty) {
        ReportBillerDialog f = new ReportBillerDialog();
        f.mContext = _context;
        f.isActivty = _isActivty;
        return f;
    }


    public ReportBillerDialog() {
        // Empty constructor required for DialogFragment
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            if(isActivty)
                callback = (OnDialogOkCallback) getActivity();
            else
                callback = (OnDialogOkCallback) getTargetFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException("Calling fragment must implement DialogClickListener interface");
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        this.dismiss();
        callback.onOkButton();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_report_biller, container);

        Bundle args = getArguments();
        Log.d("isi args", args.toString());
        String tx_status = args.getString(AppParams.TX_STATUS);
        String tx_remark = args.getString(AppParams.TX_REMARK);

        TextView tvStatus = (TextView) view.findViewById(R.id.dialog_report_transaction_remark);
        TextView tvRemark = (TextView) view.findViewById(R.id.dialog_report_transaction_remark_sub);

        if(tx_status.equalsIgnoreCase("S") || tx_status.equalsIgnoreCase("OR")) {
            tvRemark.setVisibility(View.GONE);
            if(tx_status.equalsIgnoreCase("S"))
                tvStatus.setText(getString(R.string.transaction_success));
            else if(tx_status.equalsIgnoreCase("OR"))
                tvStatus.setText(getString(R.string.transaction_pending));
            String date = args.getString(AppParams.DATE_TIME);
            String tx_id = args.getString(AppParams.TX_ID);
            String member_cust_id = args.getString(AppParams.MEMBER_CUST_ID);
            String member_cust_name = args.getString(AppParams.MEMBER_CUST_NAME);
            String bank_name = args.getString(AppParams.BANK_NAME);
            String product_name = args.getString(AppParams.PRODUCT_NAME);
            String amount = args.getString(AppParams.AMOUNT);
            String fee = args.getString(AppParams.ADMIN_FEE);
            String total = args.getString(AppParams.TOTAL_PAY);

            ScrollView layout_success = (ScrollView) view.findViewById(R.id.layout_success);
            layout_success.setVisibility(View.VISIBLE);

            TextView tvDateTime = (TextView) view.findViewById(R.id.dialog_report_date_time);
            TextView tvRef = (TextView) view.findViewById(R.id.dialog_report_tx_id);
            TextView tvMemberID = (TextView) view.findViewById(R.id.dialog_report_member_id);
            TextView tvMemberName = (TextView) view.findViewById(R.id.dialog_report_member_name);
            TextView tvBankName = (TextView) view.findViewById(R.id.dialog_report_bank_name);
            TextView tvBankProduct = (TextView) view.findViewById(R.id.dialog_report_bank_product);
            TextView tvAmount = (TextView) view.findViewById(R.id.dialog_report_amount);
            TextView tvFee = (TextView) view.findViewById(R.id.dialog_report_tx_fee);
            TextView tvTotalAmount = (TextView) view.findViewById(R.id.dialog_report_total_amount);

            tvDateTime.setText(date);
            tvRef.setText(tx_id);
            tvMemberID.setText(member_cust_id);
            tvMemberName.setText(member_cust_name);
            tvBankName.setText(bank_name);
            tvBankProduct.setText(product_name);
            tvAmount.setText(FormatCurrency.getRupiahFormat(amount));
            tvFee.setText(FormatCurrency.getRupiahFormat(fee));
            tvTotalAmount.setText(FormatCurrency.getRupiahFormat(total));
        }
        else if(tx_status.equalsIgnoreCase("SP")) {
            tvStatus.setText(getString(R.string.transaction_suspect));
            if(!tx_remark.equals("null") && !tx_remark.equals("")) {
                tvRemark.setVisibility(View.VISIBLE);
                tvRemark.setText(tx_remark);
            }
        }
        else if(tx_status.equalsIgnoreCase("F")) {
            tvStatus.setText(getString(R.string.transaction_failed));
            if(!tx_remark.equals("null") && !tx_remark.equals("")) {
                tvRemark.setVisibility(View.VISIBLE);
                tvRemark.setText(tx_remark);
            }
        }

        Button btn_ok = (Button) view.findViewById(R.id.dialog_report_btn_ok);

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        btn_ok.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        this.dismiss();
        callback.onOkButton();
    }

}
