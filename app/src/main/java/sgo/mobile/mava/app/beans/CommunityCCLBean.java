package sgo.mobile.mava.app.beans;

public class CommunityCCLBean {
    public String id     = "";
    public String code   = "";
    public String name   = "";
    public String buss_scheme_code  = "";

    public CommunityCCLBean(String id, String code, String name, String buss_scheme_code) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.buss_scheme_code = buss_scheme_code;
    }

    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBuss_scheme_code() {
        return buss_scheme_code;
    }

    public void setBuss_scheme_code(String buss_scheme_code) {
        this.buss_scheme_code = buss_scheme_code;
    }

    public String toString()
    {
        return( name  );
    }
}