package sgo.mobile.mava.app.beans;

public class MemberDGIBean {
    public String id   = "";
    public String code = "";
    public String name = "";

    public MemberDGIBean(String _id, String _code, String _name)
    {
        id = _id;
        name = _name;
        code = _code;
    }

    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String toString()
    {
        return( name  );
    }
}