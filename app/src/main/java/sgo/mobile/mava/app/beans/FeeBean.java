package sgo.mobile.mava.app.beans;

public class FeeBean {
    public String ccy_id = "";
    public String buyer_fee = "";
    public String commission_fee ="";

    public FeeBean(String ccy_id, String buyer_fee, String commission_fee) {
        this.ccy_id = ccy_id;
        this.buyer_fee = buyer_fee;
        this.commission_fee = commission_fee;
    }

    public String getCcy_id() {
        return ccy_id;
    }

    public void setCcy_id(String ccy_id) {
        this.ccy_id = ccy_id;
    }

    public String getBuyer_fee() {
        return buyer_fee;
    }

    public void setBuyer_fee(String buyer_fee) {
        this.buyer_fee = buyer_fee;
    }

    public String getCommission_fee() {
        return commission_fee;
    }

    public void setCommission_fee(String commission_fee) {
        this.commission_fee = commission_fee;
    }

    @Override
    public String toString() {
        return "FeeBean{" +
                "ccy_id='" + ccy_id + '\'' +
                ", buyer_fee='" + buyer_fee + '\'' +
                ", commission_fee='" + commission_fee + '\'' +
                '}';
    }
}
