package sgo.mobile.mava.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import sgo.mobile.mava.conf.AplConstants;
import sgo.mobile.mava.conf.AppParams;
import sgo.mobile.mava.frameworks.net.loopj.android.http.MySSLSocketFactory;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.activities.MainActivity;
import sgo.mobile.mava.app.ui.dialog.DefinedDialog;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.mava.frameworks.net.loopj.android.http.RequestParams;

public class FragmentC2CCashIn extends Fragment {
    private ProgressDialog pDialog;
    FragmentManager fm;
    String Result;

    String community_code, requester_id, sender_no, receiver_no, total, full_amount;

    EditText txt_sender_number;
    EditText txt_receiver_number;
    EditText inpAmount;
    Button btnDone;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_c2c_cashin, container, false);

        txt_sender_number   = (EditText) view.findViewById(R.id.txt_sender_number);
        txt_receiver_number = (EditText) view.findViewById(R.id.txt_receiver_number);
        inpAmount           = (EditText) view.findViewById(R.id.inpAmount);

        btnDone               = (Button) view.findViewById(R.id.btnDone);
        btnDone.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                community_code       = AplConstants.CommCodeCTC;
                requester_id         = AplConstants.MemberCodeCTC;

                sender_no      = txt_sender_number.getText().toString();
                receiver_no    = txt_receiver_number.getText().toString();

                int total_input    = Integer.parseInt(inpAmount.getText().toString());
                //int total_IDR   = total_input * AplConstants.malayRate;
                int total_IDR      = total_input;
                total              = String.valueOf(total_IDR);

                full_amount        = "Y";

                if(total.equalsIgnoreCase("") || sender_no.equalsIgnoreCase("") || receiver_no.equalsIgnoreCase(""))
                {
                    Toast.makeText(getActivity(), R.string.form_alert, Toast.LENGTH_SHORT).show();
                }else{

                    pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, "Processing C2C Cash In...");
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                    client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
                    RequestParams params   = new RequestParams();

                    params.put("community_code", community_code);
                    params.put("requester_id", requester_id);
                    params.put("sender_no", sender_no);
                    params.put("total", total);
                    params.put("receiver_no", receiver_no);
                    params.put("full_amount", full_amount);

                    Log.d("params", params.toString());
                    client.post(AplConstants.CashInC2CMobileAPI, params, new AsyncHttpResponseHandler() {
                        public void onSuccess(String content) {
                            Log.d("result:", content);
                            try {
                                JSONObject object         = new JSONObject(content);

                                String rq_uuid            = object.getString("rq_uuid");
                                String error_code         = object.getString("error_code");
                                String error_msg          = object.getString("error_msg");

                                if (pDialog != null) {
                                    pDialog.dismiss();
                                }

                                if (error_code.equals(AppParams.SUCCESS_CODE)) {

                                    String transaction_id     = object.getString("transaction_id");
                                    String community_code     = object.getString("community_code");
                                    String requester_id       = object.getString("requester_id");
                                    String sender_no          = object.getString("sender_no");
                                    String receiver_no        = object.getString("receiver_no");
                                    String total_amount       = object.getString("total");
                                    String fee_in             = object.getString("fee_in");
                                    String fee_out            = object.getString("fee_out");
                                    String trx_date           = object.getString("trx_date");
                                    String ref                = object.getString("ref");

                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                    alert.setTitle("Cash To Cash");
                                    alert.setMessage("Process Cash In Success, Sender phone will receive an OTP via sms for the next step");
                                    alert.setPositiveButton("OK", null);
                                    alert.show();

                                    Fragment newFragment = null;
                                    newFragment = new FragmentC2CConfirmToken();
                                    Bundle args = new Bundle();
                                    args.putString("transaction_id", transaction_id);
                                    args.putString("community_code", community_code);
                                    args.putString("requester_id", requester_id);
                                    args.putString("sender_no", sender_no);
                                    args.putString("receiver_no", receiver_no);
                                    args.putString("amount", total);
                                    args.putString("total", total_amount);
                                    args.putString("fee_in", fee_in);
                                    args.putString("fee_out", fee_out);
                                    args.putString("trx_date", trx_date);
                                    newFragment.setArguments(args);
                                    switchFragment(newFragment);

                                } else {
                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                    alert.setTitle("Cash To Cash");
                                    alert.setMessage("Cash To Cash : " + error_msg);
                                    alert.setPositiveButton("OK", null);
                                    alert.show();
                                }

                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        };

                        public void onFailure(Throwable error, String content) {
                            if (pDialog != null) {
                                pDialog.dismiss();
                            }
                            Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                        }
                    });

                }
            }
        });

        return view;
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }
}
