package sgo.mobile.mava.app.beans;

public class CommunityGWBean {
    public String id   = "";
    public String name = "";
    public String code = "";
    public String buss_scheme_code = "";

    public CommunityGWBean(String _id, String _name, String _code, String _buss_scheme_code)
    {
        id = _id;
        name = _name;
        code = _code;
        buss_scheme_code = _buss_scheme_code;
    }

    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getCode(){
        return code;
    }

    public String toString()
    {
        return( name  );
    }
}
