package sgo.mobile.mava.app.beans;

/**
 * Created by thinkpad on 8/28/2015.
 */
public class TrxStatusBean {

    private String tx_id;
    private String tx_amount;
    private String tx_date;
    private String tx_status;
    private String tx_remark;
    private String comm_name;
    private String member_code;
    private String bank_name;
    private String product_name;

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getComm_name() {
        return comm_name;
    }

    public void setComm_name(String comm_name) {
        this.comm_name = comm_name;
    }

    public String getMember_code() {
        return member_code;
    }

    public void setMember_code(String member_code) {
        this.member_code = member_code;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getTx_amount() {
        return tx_amount;
    }

    public void setTx_amount(String tx_amount) {
        this.tx_amount = tx_amount;
    }

    public String getTx_date() {
        return tx_date;
    }

    public void setTx_date(String tx_date) {
        this.tx_date = tx_date;
    }

    public String getTx_id() {
        return tx_id;
    }

    public void setTx_id(String tx_id) {
        this.tx_id = tx_id;
    }

    public String getTx_remark() {
        return tx_remark;
    }

    public void setTx_remark(String tx_remark) {
        this.tx_remark = tx_remark;
    }

    public String getTx_status() {
        return tx_status;
    }

    public void setTx_status(String tx_status) {
        this.tx_status = tx_status;
    }
}
