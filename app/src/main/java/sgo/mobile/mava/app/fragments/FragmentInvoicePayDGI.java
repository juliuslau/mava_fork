package sgo.mobile.mava.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.mava.AppHelper;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.activities.MainActivity;
import sgo.mobile.mava.app.adapter.InvoiceDGIPayAdapter;
import sgo.mobile.mava.app.dialogs.DialogDescription;
import sgo.mobile.mava.app.ui.dialog.DefinedDialog;
import sgo.mobile.mava.conf.AplConstants;
import sgo.mobile.mava.conf.AppParams;
import sgo.mobile.mava.frameworks.math.FormatCurrency;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.mava.frameworks.net.loopj.android.http.MySSLSocketFactory;
import sgo.mobile.mava.frameworks.net.loopj.android.http.RequestParams;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class FragmentInvoicePayDGI extends Fragment {
    private ProgressDialog pDialog;
    FragmentManager fm;

    public String member_id, member_code, member_name, session_id_param;
    public String total_pay;
    public String session_id_var;
    public String token_id, validate_token;
    public String comm_id, comm_name, comm_code, sales_alias,buss_scheme_code;
    public String ccy_id, buyer_fee, commission_fee, min_amount, max_amount;
    public String sales_id,  bank_code, product_code, seller_fee, benef_acct_no, benef_acct_name, charges_acct_no, fee_acct_no;
    public String payment_type_code, payment_type_name, payment_remark, source_acct_bank, payment_bankname = "", phone_number;
//    public String fee_multiple;

    // declare view objects
    View view;
    ListView listMenu;
    ProgressBar prgLoading;
    TextView txtAlert;
    TextView lbl_header;
    TableLayout tabel_footer;
    TextView lbl_total_pay_amount;
    Button lbl_desc;


    InvoiceDGIPayAdapter invoiceDGIAdapter;

    // create arraylist variables to store data from server
    private String partial_payment = "";
    public static ArrayList<String> doc_no_pay = new ArrayList<String>();
    public static ArrayList<String> doc_id_pay = new ArrayList<String>();
    public static ArrayList<String> amount_pay = new ArrayList<String>();
    public static ArrayList<String> remain_amount_pay = new ArrayList<String>();
    public static ArrayList<String> hold_amount_pay = new ArrayList<String>();
    public static ArrayList<String> input_amount_pay = new ArrayList<String>();
    public static ArrayList<String> ccy_pay = new ArrayList<String>();
    public static ArrayList<String> doc_desc_pay = new ArrayList<String>();
    public static ArrayList<String> due_date_pay = new ArrayList<String>();
    public static ArrayList<String> session_id_pay = new ArrayList<String>();

    EditText txtOtp;
    Button btnDone;
    Button btnCancel;
    Button btnResendToken;

    // create price format
    DecimalFormat formatData = new DecimalFormat("#.##");

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view  = inflater.inflate(R.layout.fragment_invdgi_confirm, container, false);
        prgLoading = (ProgressBar) view.findViewById(R.id.prgLoading);
        listMenu   = (ListView) view.findViewById(R.id.listMenu);
        tabel_footer = (TableLayout) view.findViewById(R.id.tabel_footer);
        lbl_total_pay_amount = (TextView) view.findViewById(R.id.lbl_total_pay_amount);
        lbl_desc = (Button) view.findViewById(R.id.btn_desc);

        txtOtp                = (EditText) view.findViewById(R.id.txtOtp);
        btnDone               = (Button) view.findViewById(R.id.btnDone);
        btnCancel             = (Button) view.findViewById(R.id.btnCancel);
        btnResendToken        = (Button) view.findViewById(R.id.btnResend);

        Bundle bundle         = this.getArguments();
        member_id             = bundle.getString("member_id");
        member_code           = bundle.getString("member_code");
        member_name           = bundle.getString("member_name");
        session_id_param      = bundle.getString("session_id_param");
        session_id_param      = (session_id_param.equalsIgnoreCase("null")) ? "" : session_id_param;

        ccy_id                = bundle.getString("ccy_id");
        buyer_fee             = bundle.getString("buyer_fee");
        seller_fee            = bundle.getString("seller_fee");
        commission_fee        = bundle.getString("commission_fee");
        min_amount            = bundle.getString("min_amount");
        max_amount            = bundle.getString("max_amount");

        comm_id               = bundle.getString("comm_id");
        comm_name             = bundle.getString("comm_name");
        comm_code             = bundle.getString("comm_code");
        sales_alias           = bundle.getString("sales_alias");
        buss_scheme_code      = bundle.getString("buss_scheme_code");

        payment_type_code     = bundle.getString("payment_type_code");
        payment_type_name     = bundle.getString("payment_type_name");
        payment_remark        = bundle.getString("payment_remark");
        source_acct_bank      = bundle.getString("source_acct_bank");
        payment_bankname      = bundle.getString("payment_bank_name");
        phone_number          = bundle.getString("phone_number");
//        fee_multiple          = bundle.getString("fee_multiple");

        lbl_header = (TextView) view.findViewById(R.id.label_header);
        lbl_header.setText(member_name);

        invoiceDGIAdapter = new InvoiceDGIPayAdapter(getActivity());
        parseJSONData();

        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                validate_token        = txtOtp.getText().toString();
                if(validate_token.equalsIgnoreCase(""))
                {
                    Toast.makeText(getActivity(), R.string.form_alert, Toast.LENGTH_SHORT).show();
                }else{

                    pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, "Memproses Token...");
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                    client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
                    RequestParams params   = new RequestParams();

                    final String param_sales_id = AppHelper.getUserId(getActivity());
                    params.put("sales_id", param_sales_id);
                    params.put("member_id", member_id);
                    params.put("token_id", token_id);
                    params.put("validate_token", validate_token);

                    Log.d("params", params.toString());
                    client.post(AplConstants.ConfirmTokenDiMobileAPI, params, new AsyncHttpResponseHandler() {
                        public void onSuccess(String content) {
                            Log.d("result:", content);
                            try {
                                JSONObject object         = new JSONObject(content);

                                String error_code         = object.getString("error_code");
                                String error_msg          = object.getString("error_message");

                                if (pDialog != null) {
                                    pDialog.dismiss();
                                }

                                if (error_code.equals(AppParams.SUCCESS_CODE)) {
                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                    alert.setTitle("Konfirmasi Token Digital Inclusion");
                                    alert.setMessage("Digital Inclusion : Sukses");
                                    alert.setPositiveButton("OK", null);
                                    alert.show();

                                    hideKeyboard();

                                    Fragment newFragment = null;
                                    newFragment = new FragmentMemberListDGI();
                                    Bundle args = new Bundle();
                                    args.putString("comm_id", comm_id);
                                    args.putString("comm_name", comm_name);
                                    args.putString("comm_code", comm_code);
                                    args.putString("sales_alias", sales_alias);
                                    args.putString("buss_scheme_code", buss_scheme_code);

                                    newFragment.setArguments(args);
                                    switchFragment(newFragment);

                                } else {
                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                    alert.setTitle("Keterangan Token Digital Inclusion");
                                    alert.setMessage("Digital Inclusion : " + error_msg);
                                    alert.setPositiveButton("OK", null);
                                    alert.show();
                                }

                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        };

                        public void onFailure(Throwable error, String content) {
                            if (pDialog != null) {
                                pDialog.dismiss();
                            }
                            Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });


        btnResendToken.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {

                pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, "Meminta ulang Token...");
                AsyncHttpClient client = new AsyncHttpClient();
                client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
                RequestParams params   = new RequestParams();

                String param_sales_id = AppHelper.getUserId(getActivity());
                params.put("sales_id", param_sales_id);
                params.put("member_id", member_id);
                params.put("token_id", token_id);
                Log.d("params", params.toString());
                client.post(AplConstants.ResendTokenDiMobileAPI, params, new AsyncHttpResponseHandler() {
                    public void onSuccess(String content) {
                        Log.d("result:", content);
                        try {
                            JSONObject object         = new JSONObject(content);

                            String error_code         = object.getString("error_code");
                            String error_msg          = object.getString("error_message");

                            if (pDialog != null) {
                                pDialog.dismiss();
                            }

                            if (error_code.equals(AppParams.SUCCESS_CODE)) {
                                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                alert.setTitle("Resend Token Digital Inclusion");
                                alert.setMessage("Digital Inclusion : Meminta ulang Token Sukses");
                                alert.setPositiveButton("OK", null);
                                alert.show();
                            } else {
                                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                alert.setTitle("Resend Token Digital Inclusion");
                                alert.setMessage("Digital Inclusion : " + error_msg);
                                alert.setPositiveButton("OK", null);
                                alert.show();
                            }

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    };

                    public void onFailure(Throwable error, String content) {
                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
                        Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });


        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                hideKeyboard();
                Fragment newFragment = null;
                newFragment = new FragmentInvoiceListDGI();
                Bundle args = new Bundle();
                args.putString("member_id", member_id);
                args.putString("member_code", member_code);
                args.putString("member_name", member_name);
                args.putString("session_id_param", session_id_param);

                args.putString("comm_id", comm_id);
                args.putString("comm_name", comm_name);
                args.putString("comm_code", comm_code);
                args.putString("sales_alias", sales_alias);
                args.putString("buss_scheme_code", buss_scheme_code);

                newFragment.setArguments(args);
                switchFragment(newFragment);
            }
        });

        return view;
    }

    public void parseJSONData(){
        clearData();
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(AplConstants.HTTP_LONG_TIMEOUT);
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        RequestParams params = new RequestParams();

        String param_sales_id = AppHelper.getUserId(getActivity());
        params.put("member_id", member_id);
        params.put("sales_id", param_sales_id);
        params.put("session_id", session_id_param);
        params.put("ccy_id", ccy_id);
        params.put("buyer_fee", buyer_fee);
        params.put("commission_fee", commission_fee);
        params.put("min_amount", min_amount);
        params.put("max_amount", max_amount);
        params.put("source_acct_bank", source_acct_bank);

        params.put("payment_type", payment_type_code);
        params.put("payment_remark", payment_remark);
        params.put("phone_no", phone_number);
        params.put("seller_fee", seller_fee);

        Log.d("params", params.toString());
        client.post(AplConstants.DiListPayMobileAPI, params, new AsyncHttpResponseHandler() {
            public void onSuccess(String content) {
                Log.d("result:", content);
                try {

                    JSONObject json = new JSONObject(content);

                    String error_code         = json.getString("error_code");
                    String error_msg          = json.getString("error_message");

                    if (error_code.equals(AppParams.SUCCESS_CODE)) {
                        JSONArray data = json.getJSONArray("invoice_data"); // this is the "items: [ ] part

                        partial_payment = json.getString("partial_payment");
                        total_pay = json.getString("total_pay");
                        session_id_var = json.getString("session_id_var");
                        token_id = json.getString("token_id");

                        if(payment_type_code.equalsIgnoreCase(AppParams.CT)) {
                            if(payment_remark.length() == 0) {
                                payment_remark = "No Remark";
                            }
                        }
                        lbl_desc.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                FragmentManager fragmentManager = getFragmentManager();
                                DialogDescription dialog = new DialogDescription();
                                Bundle args = new Bundle();
                                args.putString("payment_type_code", payment_type_code);
                                args.putString("payment_type_name", payment_type_name);
                                args.putString("payment_bankname", payment_bankname);
                                args.putString("payment_remark", payment_remark);
                                args.putString("phone_number", phone_number);
                                dialog.setArguments(args);
                                dialog.setCancelable(true);
                                dialog.setTargetFragment(FragmentInvoicePayDGI.this, 100);
                                dialog.show(fragmentManager, "dialogDescription");
                            }
                        });

                        for (int i = 0; i < data.length(); i++) {
                            JSONObject object = data.getJSONObject(i);
                            doc_no_pay.add(object.getString("doc_no"));
                            doc_id_pay.add(object.getString("doc_id"));
                            amount_pay.add(object.getString("amount"));
                            remain_amount_pay.add(object.getString("remain_amount"));
                            hold_amount_pay.add(object.getString("hold_amount"));
                            ccy_pay.add(object.getString("ccy"));
                            doc_desc_pay.add(object.getString("doc_desc"));
                            due_date_pay.add(object.getString("due_date"));
                            input_amount_pay.add(object.getString("input_amount"));
                            session_id_pay.add(object.getString("session_id"));
                        }

                        String str_fee_payment = json.getString("admin_fee");
//                        str_fee_payment             = (str_fee_payment != null && !str_fee_payment.equals("") && !str_fee_payment.equals("null") && Integer.parseInt(str_fee_payment) > 0)? str_fee_payment : "0";

//                        int fee_payment = Integer.parseInt(buyer_fee) + Integer.parseInt(commission_fee);
//                        String str_fee_payment = null;
//                        if(payment_type_code.equalsIgnoreCase(AppParams.CT)) {
//                            str_fee_payment = Integer.toString(fee_payment);
//                        }
//                        else if(payment_type_code.equalsIgnoreCase(AppParams.BG) || payment_type_code.equalsIgnoreCase(AppParams.TS)){
//                            if (fee_multiple.equalsIgnoreCase("N"))
//                                str_fee_payment = Integer.toString(fee_payment);
//                            else if (fee_multiple.equalsIgnoreCase("Y"))
//                                str_fee_payment = Integer.toString(fee_payment * doc_no_pay.size());
//                        }

                        int total_pay_fee = Integer.parseInt(total_pay) + Integer.parseInt(str_fee_payment);
                        String str_total_fee = Integer.toString(total_pay_fee);

                        String feePayment = (str_fee_payment != null && !str_fee_payment.equals("") && !str_fee_payment.equals("null")) ? FormatCurrency.getRupiahFormat(str_fee_payment) : "";
                        String str_format_total = FormatCurrency.getRupiahFormat(str_total_fee) + "  (Fee: " + feePayment + ")";
                        String total_payment = (str_total_fee != null && !str_total_fee.equals("") && !str_total_fee.equals("null")) ? str_format_total : "";
                        lbl_total_pay_amount.setText(total_payment);

                        // when finish parsing, hide progressbar
                        prgLoading.setVisibility(View.GONE);

                        // if data available show data on list
                        // otherwise, show alert text
                        if (doc_no_pay.size() > 0) {
                            listMenu.setVisibility(View.VISIBLE);
                            listMenu.setAdapter(invoiceDGIAdapter);
                            lbl_header.setVisibility(View.VISIBLE);
                            tabel_footer.setVisibility(View.VISIBLE);
                        } else {
                            txtAlert.setVisibility(View.VISIBLE);
                        }
                    }else{
                        AlertDialog.Builder alert = new AlertDialog.Builder(view.getContext());
                        alert.setTitle("Keterangan Token Digital Inclusion");
                        alert.setMessage("Digital Inclusion : " + error_msg);
                        alert.setPositiveButton("OK", null);
                        alert.show();

                        Fragment newFragment = null;
                        newFragment = new FragmentInvoiceListDGI();
                        Bundle args = new Bundle();
                        args.putString("member_id", member_id);
                        args.putString("member_code", member_code);
                        args.putString("member_name", member_name);
                        args.putString("session_id_param", session_id_param);

                        args.putString("comm_id", comm_id);
                        args.putString("comm_name", comm_name);
                        args.putString("comm_code", comm_code);
                        args.putString("sales_alias", sales_alias);
                        args.putString("buss_scheme_code", buss_scheme_code);

                        newFragment.setArguments(args);
                        switchFragment(newFragment);

                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                }
            };

            public void onFailure(Throwable error, String content) {
                Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
            }
        });
    }

    // clear arraylist variables before used
    void clearData(){
        doc_no_pay.clear();
        doc_id_pay.clear();
        amount_pay.clear();
        remain_amount_pay.clear();
        hold_amount_pay.clear();
        ccy_pay.clear();
        doc_desc_pay.clear();
        due_date_pay.clear();
        input_amount_pay.clear();
        session_id_pay.clear();
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }


    public void hideKeyboard(){
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}