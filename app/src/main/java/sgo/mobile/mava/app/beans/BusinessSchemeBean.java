package sgo.mobile.mava.app.beans;

public class BusinessSchemeBean {
    public String code   = "";
    public String name = "";

    public BusinessSchemeBean( String _code, String _name )
    {
        code = _code;
        name = _name;
    }

    public String getCode(){
        return code;
    }

    public void setCode(String code){
        this.code = code;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String toString()
    {
        return( name  );
    }
}