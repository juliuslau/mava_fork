package sgo.mobile.mava.app.beans;

public class MemberCCLDGIBean {
    public String id   = "";
    public String code = "";

    public MemberCCLDGIBean(String _id, String _code)
    {
        id = _id;
        code = _code;
    }

    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String toString()
    {
        return( code  );
    }
}