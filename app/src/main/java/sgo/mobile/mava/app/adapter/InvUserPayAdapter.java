package sgo.mobile.mava.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.fragments.FragmentInvUserPay;
import sgo.mobile.mava.frameworks.math.FormatCurrency;

public class InvUserPayAdapter extends BaseAdapter {
    private Activity activity;

    public InvUserPayAdapter(Activity act) {
        this.activity = act;
    }
    public int getCount() {
        // TODO Auto-generated method stub
        return FragmentInvUserPay.doc_no_pay.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_invdgipay_list_item, null);
            holder = new ViewHolder();

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        String InputAmount = FragmentInvUserPay.input_amount_pay.get(position);
        int remain_amount = Integer.parseInt(FragmentInvUserPay.remain_amount_pay.get(position));
        holder.txtText    = (TextView) convertView.findViewById(R.id.txtText);
        holder.txtSubText = (TextView) convertView.findViewById(R.id.txtSubText);
        holder.txtSubText2 = (TextView) convertView.findViewById(R.id.txtSubText2);

        holder.txtText.setText("Invoice " + FragmentInvUserPay.doc_no_pay.get(position));
        holder.txtSubText.setText( "Sisa : " +  FormatCurrency.getRupiahFormat(Integer.toString(remain_amount)));

        if (InputAmount != null && !InputAmount.equals("") && !InputAmount.equals("null") && Integer.parseInt(InputAmount) > 0){
            String buyer_fee      = FragmentInvUserPay.buyer_fee;
            buyer_fee             = (buyer_fee != null && !buyer_fee.equals("") && !buyer_fee.equals("null") && Integer.parseInt(buyer_fee) > 0)? buyer_fee : "0";
            int total_pay_plus_fee     = Integer.parseInt(InputAmount) + Integer.parseInt(buyer_fee);
            holder.txtSubText2.setVisibility(View.VISIBLE);
            holder.txtSubText2.setText( "Bayar : " +  FormatCurrency.getRupiahFormat(Integer.toString(total_pay_plus_fee)) + " (Fee : " + FormatCurrency.getRupiahFormat(buyer_fee) + ")");
        }else{
            holder.txtSubText2.setVisibility(View.GONE);
        }

        return convertView;
    }

    static class ViewHolder {
        TextView txtText, txtSubText, txtSubText2;
    }
}
