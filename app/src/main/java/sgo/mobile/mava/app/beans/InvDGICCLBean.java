package sgo.mobile.mava.app.beans;

public class InvDGICCLBean {
    public String hold_id  = "";
    public String hold_amount = "";
    public String ccy_id  = "";

    public InvDGICCLBean(String hold_id, String hold_amount, String ccy_id) {
        this.hold_id = hold_id;
        this.hold_amount = hold_amount;
        this.ccy_id = ccy_id;
    }

    public String getHold_id() {
        return hold_id;
    }

    public void setHold_id(String hold_id) {
        this.hold_id = hold_id;
    }

    public String getHold_amount() {
        return hold_amount;
    }

    public void setHold_amount(String hold_amount) {
        this.hold_amount = hold_amount;
    }

    public String getCcy_id() {
        return ccy_id;
    }

    public void setCcy_id(String ccy_id) {
        this.ccy_id = ccy_id;
    }

    @Override
    public String toString() {
        return "InvDGICCLBean{" +
                "hold_id='" + hold_id + '\'' +
                '}';
    }
}
