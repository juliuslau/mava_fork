package sgo.mobile.mava.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import sgo.mobile.mava.conf.AplConstants;
import sgo.mobile.mava.conf.AppParams;
import sgo.mobile.mava.frameworks.net.loopj.android.http.MySSLSocketFactory;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.activities.MainActivity;
import sgo.mobile.mava.app.ui.dialog.DefinedDialog;
import sgo.mobile.mava.frameworks.math.FormatCurrency;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.mava.frameworks.net.loopj.android.http.RequestParams;

public class FragmentC2CConfirmToken extends Fragment {
    private ProgressDialog pDialog;
    FragmentManager fm;
    String Result;
    String transaction_id, transfer_code, community_code, requester_id, sender_no, receiver_no, total, amount, fee_in, fee_out, trx_date;

    EditText txtOtp;
    Button btnDone;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_c2c_confimtoken, container, false);

        txtOtp                = (EditText) view.findViewById(R.id.txtOtp);
        btnDone               = (Button) view.findViewById(R.id.btnDone);

        Bundle bundle         = this.getArguments();
        transaction_id        = bundle.getString("transaction_id");
        community_code        = bundle.getString("community_code");
        requester_id          = bundle.getString("requester_id");
        sender_no             = bundle.getString("sender_no");
        receiver_no           = bundle.getString("receiver_no");
        total                 = bundle.getString("total");
        total                 = FormatCurrency.getRupiahFormat(total);
        amount                = bundle.getString("amount");
        amount                = FormatCurrency.getRupiahFormat(amount);
        fee_in                = bundle.getString("fee_in");
        fee_in                = FormatCurrency.getRupiahFormat(fee_in);
        fee_out               = bundle.getString("fee_out");
        fee_out               = FormatCurrency.getRupiahFormat(fee_out);
        trx_date              = bundle.getString("trx_date");


        TextView lbl_nomor_pengirim = (TextView) view.findViewById(R.id.lbl_nomor_pengirim);
        lbl_nomor_pengirim.setText(sender_no);

        TextView lbl_nomor_Penerima = (TextView) view.findViewById(R.id.lbl_nomor_Penerima);
        lbl_nomor_Penerima.setText(receiver_no);

        TextView lbl_jumlah         = (TextView) view.findViewById(R.id.lbl_jumlah);
        lbl_jumlah.setText(amount);

        TextView lbl_fee_masuk      = (TextView) view.findViewById(R.id.lbl_fee_masuk);
        lbl_fee_masuk.setText(fee_in);

        TextView lbl_fee_keluar     = (TextView) view.findViewById(R.id.lbl_fee_keluar);
        lbl_fee_keluar.setText(fee_out);

        TextView lbl_jumlah_total    = (TextView) view.findViewById(R.id.lbl_jumlah_total);
        lbl_jumlah_total.setText(total);

        TextView lbl_tanggal_transaksi    = (TextView) view.findViewById(R.id.lbl_tanggal_transaksi);
        lbl_tanggal_transaksi.setText(trx_date);

        btnDone.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                // TODO Auto-generated method stub


                transfer_code        = txtOtp.getText().toString();
                if(transfer_code.equalsIgnoreCase(""))
                {
                    Toast.makeText(getActivity(), R.string.form_alert, Toast.LENGTH_SHORT).show();
                }else{

                    pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, "Processing Token C2C...");
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                    client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
                    RequestParams params   = new RequestParams();

                    params.put("transaction_id", transaction_id);
                    params.put("transfer_code", transfer_code);

                    Log.d("params", params.toString());
                    client.post(AplConstants.ConfirmTokenC2CMobileAPI, params, new AsyncHttpResponseHandler() {
                        public void onSuccess(String content) {
                            Log.d("result:", content);
                            try {
                                JSONObject object         = new JSONObject(content);

                                String rq_uuid            = object.getString("rq_uuid");
                                String error_code         = object.getString("error_code");
                                String error_msg          = object.getString("error_msg");
                                String transaction_id     = object.getString("transaction_id");
                                String ref                = object.getString("ref");

                                if (pDialog != null) {
                                    pDialog.dismiss();
                                }

                                if (error_code.equals(AppParams.SUCCESS_CODE)) {
                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                    alert.setTitle("Cash to Cash");
                                    alert.setMessage("Cash to Cash : " + error_msg);
                                    alert.setPositiveButton("OK", null);
                                    alert.show();

                                    Fragment newFragment = null;
                                    //newFragment = new FragmentC2C();
                                    newFragment = new FragmentC2CCashOut();
                                    switchFragment(newFragment);

                                } else {
                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                    alert.setTitle("Cash to Cash");
                                    alert.setMessage("Cash to Cash : " + error_msg);
                                    alert.setPositiveButton("OK", null);
                                    alert.show();
                                }

                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        };

                        public void onFailure(Throwable error, String content) {
                            if (pDialog != null) {
                                pDialog.dismiss();
                            }
                            Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });

        return view;
    }


    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }
}
