package sgo.mobile.mava.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;

import java.util.List;

import sgo.mobile.mava.R;
import sgo.mobile.mava.app.beans.InvoiceRepaymentBean;
import sgo.mobile.mava.frameworks.math.FormatCurrency;

/**
 * Created by thinkpad on 1/19/2016.
 */
public class InvoiceRepaymentAdapter extends BaseAdapter {

    private final List<InvoiceRepaymentBean> list;
    private final Activity context;
    private boolean cbVisible;

    public InvoiceRepaymentAdapter(Activity context, List<InvoiceRepaymentBean> list, boolean cbVisible) {
        this.context = context;
        this.list = list;
        this.cbVisible = cbVisible;
    }

    static class ViewHolder {
        protected TextView txtTxId, txtAmount;
        protected CheckBox checkbox;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.invoice_repayment_list_item, null);

            holder = new ViewHolder();
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtTxId = (TextView) convertView.findViewById(R.id.txtTxId);
        holder.txtAmount = (TextView) convertView.findViewById(R.id.txtAmount);
        holder.checkbox = (CheckBox) convertView.findViewById(R.id.cbInvoice);

        if(cbVisible) holder.checkbox.setVisibility(View.VISIBLE);
        else holder.checkbox.setVisibility(View.GONE);
        holder.checkbox
                .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        String selected = "0";
                        if(isChecked) selected = "1";
                        else selected = "0";

                        InvoiceRepaymentBean element = (InvoiceRepaymentBean) holder.checkbox
                                .getTag();
                        element.setSelected(selected);

                        ActiveAndroid.beginTransaction();
                        InvoiceRepaymentBean.updateSelected(selected, list.get(position).getTx_id());
                        ActiveAndroid.setTransactionSuccessful();
                        ActiveAndroid.endTransaction();

                    }
                });

        holder.checkbox.setTag(list.get(position));

        holder.txtTxId.setText(list.get(position).getTx_id());
        holder.txtAmount.setText("Jumlah : " + FormatCurrency.getRupiahFormat(list.get(position).getAmount()));
        if(list.get(position).getSelected().equals("1")) {
            holder.checkbox.setChecked(true);
        }
        else {
            holder.checkbox.setChecked(false);
        }
        return convertView;
    }
}
