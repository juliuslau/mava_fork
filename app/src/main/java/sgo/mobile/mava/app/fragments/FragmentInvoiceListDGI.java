package sgo.mobile.mava.app.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.securepreferences.SecurePreferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

import sgo.mobile.mava.AppHelper;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.activities.MainActivity;
import sgo.mobile.mava.app.adapter.InvoiceDGIAdapter;
import sgo.mobile.mava.app.adapter.PaymentTypeAdapter;
import sgo.mobile.mava.app.beans.PaymentTypeBean;
import sgo.mobile.mava.app.beans.SelectAgainSpinner;
import sgo.mobile.mava.app.dialogs.DialogFragmentDGISales;
import sgo.mobile.mava.conf.AplConstants;
import sgo.mobile.mava.conf.AppParams;
import sgo.mobile.mava.frameworks.math.FormatCurrency;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.mava.frameworks.net.loopj.android.http.MySSLSocketFactory;
import sgo.mobile.mava.frameworks.net.loopj.android.http.RequestParams;

import static android.view.View.OnClickListener;

public class FragmentInvoiceListDGI extends Fragment implements DialogFragmentDGISales.NoticeDialogListener {
    private ProgressDialog pDialog;
    FragmentManager fm;
    SecurePreferences sp;

    public String member_id, member_code, member_name, session_id_param;
    public String total_pay;
    public String session_id_var;
    public String ccy_id, buyer_fee, seller_fee, commission_fee, min_amount, max_amount;
    public String comm_id, comm_name, comm_code, sales_alias, buss_scheme_code;
    public String product_values;

    Bundle args;
    Fragment newFragment;

    boolean isSMSBanking = false;

    private ArrayList<PaymentTypeBean> PaymentTypeList = new ArrayList<PaymentTypeBean>();
    private static SelectAgainSpinner cbo_payment_type      = null;
    public String payment_type_code = "",
                  payment_type_name = null,
                  payment_remark = null,
                  payment_bankname = null,
                  payment_bankcode = null,
                  payment_productcode = null,
                  payment_productname = null,
                  payment_product_h2h = null;
//    public String fee_multiple = null;

    // declare view objects
    ListView listMenu;
    ProgressBar prgLoading;
    TextView txtAlert;
    TextView lbl_header;
    TableLayout tabel_footer;
    TableRow row_phone;
    TextView lbl_total_pay_amount;

    InvoiceDGIAdapter invoiceDGIAdapter;

    // create arraylist variables to store data from server
    private String partial_payment = "";
    public static ArrayList<String> doc_no = new ArrayList<String>();
    public static ArrayList<String> doc_id = new ArrayList<String>();
    public static ArrayList<String> amount = new ArrayList<String>();
    public static ArrayList<String> remain_amount = new ArrayList<String>();
    public static ArrayList<String> hold_amount = new ArrayList<String>();
    public static ArrayList<String> input_amount = new ArrayList<String>();
    public static ArrayList<String> ccy = new ArrayList<String>();
    public static ArrayList<String> doc_desc = new ArrayList<String>();
    public static ArrayList<String> due_date = new ArrayList<String>();
    public static ArrayList<String> session_id = new ArrayList<String>();

    Button btnDone;
    Button btnCancel;
    Button btnBack;

    // create price format
    DecimalFormat formatData = new DecimalFormat("#.##");

    DialogFragmentDGISales dialog;
    boolean alertShow = false;

    //mobile phone
    private ArrayList<PaymentTypeBean> listPhone = new ArrayList<PaymentTypeBean>();
    private static Spinner cbo_phone_number      = null;
    private String phone_number = null, phone_type;

    Activity activity;
    String bankEspay;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_invdgi_list, container, false);

        activity = getActivity();
        sp = new SecurePreferences(activity);
        bankEspay = sp.getString(AppParams.BANK_ESPAY,"");

        txtAlert = (TextView) view.findViewById(R.id.txtAlert);
        prgLoading = (ProgressBar) view.findViewById(R.id.prgLoading);
        listMenu   = (ListView) view.findViewById(R.id.listMenu);
        tabel_footer = (TableLayout) view.findViewById(R.id.tabel_footer);
        row_phone = (TableRow) view.findViewById(R.id.row_phone);
        lbl_total_pay_amount = (TextView) view.findViewById(R.id.lbl_total_pay_amount);

        btnDone               = (Button) view.findViewById(R.id.btnDone);
        btnCancel             = (Button) view.findViewById(R.id.btnCancel);
        btnBack               = (Button) view.findViewById(R.id.btnBack);

        cbo_payment_type      = (SelectAgainSpinner)view.findViewById(R.id.cbo_payment_type);
        cbo_phone_number      = (Spinner)view.findViewById(R.id.cbo_phone_number);

        Bundle bundle         = this.getArguments();
        member_id             = bundle.getString("member_id");
        member_code           = bundle.getString("member_code");
        member_name           = bundle.getString("member_name");
        session_id_param      = bundle.getString("session_id_param");
        session_id_param      = (session_id_param.equalsIgnoreCase("null")) ? "" : session_id_param;

        comm_id               = bundle.getString("comm_id");
        comm_name             = bundle.getString("comm_name");
        comm_code             = bundle.getString("comm_code");
        sales_alias           = bundle.getString("sales_alias");
        buss_scheme_code      = bundle.getString("buss_scheme_code");

        lbl_header = (TextView) view.findViewById(R.id.label_header);
        lbl_header.setText(member_name);

        invoiceDGIAdapter = new InvoiceDGIAdapter(activity);
        parseJSONData();

        // event listener to handle list when clicked
        listMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {

                Fragment newFragment = new FragmentInvoicePaymentDGI();
                Bundle args = new Bundle();
                args.putString("doc_no", doc_no.get(position));
                args.putString("doc_id", doc_id.get(position));
                args.putString("amount", amount.get(position));
                args.putString("remain_amount", remain_amount.get(position));
                args.putString("hold_amount", hold_amount.get(position));
                args.putString("input_amount", input_amount.get(position));
                args.putString("ccy", ccy.get(position));
                args.putString("doc_desc", doc_desc.get(position));
                args.putString("due_date", due_date.get(position));
                args.putString("partial_payment", partial_payment);
                args.putString("session_id", session_id.get(position));
                args.putString("member_id", member_id);
                args.putString("member_code", member_code);
                args.putString("member_name", member_name);

                args.putString("ccy_id", ccy_id);
                args.putString("buyer_fee", buyer_fee);
                args.putString("commission_fee", commission_fee);
                args.putString("min_amount", min_amount);
                args.putString("max_amount", max_amount);

                args.putString("comm_id", comm_id);
                args.putString("comm_name", comm_name);
                args.putString("comm_code", comm_code);
                args.putString("sales_alias", sales_alias);
                args.putString("buss_scheme_code", buss_scheme_code);

                newFragment.setArguments(args);
                switchFragment(newFragment);
            }
        });

        btnDone.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {
                CheckOutAction();
            }
        });


        btnCancel.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {
                Fragment newFragment = new FragmentInvoiceListDGI();
                Bundle args = new Bundle();
                args.putString("member_id", member_id);
                args.putString("member_code", member_code);
                args.putString("member_name", member_name);
                args.putString("session_id_param", "null");

                args.putString("comm_id", comm_id);
                args.putString("comm_name", comm_name);
                args.putString("comm_code", comm_code);
                args.putString("sales_alias", sales_alias);
                args.putString("buss_scheme_code", buss_scheme_code);

                newFragment.setArguments(args);
                switchFragment(newFragment);
            }
        });

        btnBack.setOnClickListener(new OnClickListener() {
            public void onClick(View arg0) {
                Fragment newFragment = new FragmentMemberListDGI();
                Bundle args = new Bundle();
                args.putString("comm_id", comm_id);
                args.putString("comm_name", comm_name);
                args.putString("comm_code", comm_code);
                args.putString("sales_alias", sales_alias);
                args.putString("buss_scheme_code", buss_scheme_code);

                newFragment.setArguments(args);
                switchFragment(newFragment);
            }
        });

        return view;
    }

    private void CheckOutAction() {
        if(payment_type_code.equalsIgnoreCase(AppParams.ESPAYSMS) || payment_type_code.equalsIgnoreCase(AppParams.ESPAYIB)) {
            if (total_pay != null && !total_pay.equals("") && !total_pay.equals("null") && Double.parseDouble(total_pay) > 0 &&
                    payment_type_code != "") {
                PaymentTypeBean paymentSelected = (PaymentTypeBean) cbo_payment_type.getSelectedItem();
                payment_type_code = String.valueOf(paymentSelected.payment_type);
                payment_type_name = String.valueOf(paymentSelected.payment_name);

                String param_sales_id = AppHelper.getUserId(activity);

                newFragment = null;
                newFragment = new FragmentPreviewTransaksi();
                args = new Bundle();
                args.putString("login_type", AppParams.SALES);
                args.putString("comm_id", comm_id);
                args.putString("sales_id", param_sales_id);
                args.putString("bank_code", payment_bankcode);
                args.putString("product_code", payment_productcode);
                args.putString("ccy_id", ccy_id);
                args.putString("session_id", session_id_param);
                args.putString("product_code", payment_productcode);
                args.putString("product_h2h", payment_product_h2h);
                args.putString("product_value", product_values);

                args.putString("member_id", member_id);
                args.putString("member_code", member_code);
                args.putString("member_name", member_name);
                args.putString("session_id_param", session_id_var);

                args.putString("comm_name", comm_name);
                args.putString("comm_code", comm_code);
                args.putString("sales_alias", sales_alias);
                args.putString("buss_scheme_code", buss_scheme_code);
                args.putBoolean("is_smsbanking",isSMSBanking);
                newFragment.setArguments(args);
                switchFragment(newFragment);

            } else if (payment_type_code == "") {
                Toast.makeText(getActivity(), "Checkout tidak dapat dilakukan bila belum ada item yang diisi!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), "Checkout tidak dapat dilakukan bila belum ada item yang diisi!", Toast.LENGTH_SHORT).show();
            }
        }
        else {
            if (total_pay != null && !total_pay.equals("") && !total_pay.equals("null") && Double.parseDouble(total_pay) > 0 &&
                    phone_number != null && !payment_type_code.equals("")) {
                PaymentTypeBean paymentSelected = (PaymentTypeBean) cbo_payment_type.getSelectedItem();
                payment_type_code = String.valueOf(paymentSelected.payment_type);
                payment_type_name = String.valueOf(paymentSelected.payment_name);

                Fragment newFragment = null;
                newFragment = new FragmentInvoicePayDGI();
                Bundle args = new Bundle();
                args.putString("member_id", member_id);
                args.putString("member_code", member_code);
                args.putString("member_name", member_name);
                args.putString("session_id_param", session_id_var);
                args.putString("ccy_id", ccy_id);
                args.putString("buyer_fee", buyer_fee);
                args.putString("commission_fee", commission_fee);
                args.putString("min_amount", min_amount);
                args.putString("max_amount", max_amount);

                args.putString("comm_id", comm_id);
                args.putString("comm_name", comm_name);
                args.putString("comm_code", comm_code);
                args.putString("sales_alias", sales_alias);
                args.putString("buss_scheme_code", buss_scheme_code);

                args.putString("payment_type_code", payment_type_code);
                args.putString("payment_type_name", payment_type_name);
                args.putString("payment_remark", payment_remark);
                args.putString("source_acct_bank", payment_bankcode);
                args.putString("payment_bank_name", payment_bankname);
                args.putString("phone_number", phone_number);
                args.putString("seller_fee", seller_fee);
//                        args.putString("fee_multiple", fee_multiple);

                newFragment.setArguments(args);
                switchFragment(newFragment);

            } else if (payment_type_code.equals("")) {
                Toast.makeText(getActivity(), "Checkout tidak dapat dilakukan bila belum ada item yang diisi!", Toast.LENGTH_SHORT).show();
            } else if (phone_number == null) {
                Toast.makeText(getActivity(), "Checkout tidak dapat dilakukan bila nomor HP belum dipilih!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), "Checkout tidak dapat dilakukan bila belum ada item yang diisi!", Toast.LENGTH_SHORT).show();
            }
        }
    }


    public void parseJSONData(){
        clearData();
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        client.setTimeout(AplConstants.HTTP_LONG_TIMEOUT);
        RequestParams params = new RequestParams();

        String param_sales_id = AppHelper.getUserId(activity);
        params.put("member_id", member_id);
        params.put("sales_id", param_sales_id);
        params.put("session_id", session_id_param);

        Log.d("params", params.toString());
        client.post(AplConstants.DiListMobileAPI, params, new AsyncHttpResponseHandler() {
            public void onSuccess(String content) {
                Log.d("result:", content);
                try {
                    // parse json data and store into arraylist variables
                    JSONObject json = new JSONObject(content);
                    //Toast.makeText(getActivity(), content, Toast.LENGTH_LONG).show();
                    String error_code         = json.getString("error_code");
                    String error_message      = json.getString("error_message");

                    if (error_code.equals(AppParams.SUCCESS_CODE)) {

//                        fee_multiple = json.getString(AppParams.FEE_MULTIPLE);

                        String paymentType = json.getString(AppParams.PAYMENT_TYPE);
                        Boolean adaIB = false;
                        if(!bankEspay.equals("")) {

                            JSONArray arrBankEspay = new JSONArray(bankEspay);
                            for (int i = 0; i < arrBankEspay.length(); i++) {
                                if(arrBankEspay.getJSONObject(i).getString(AppParams.PRODUCT_H2H).equalsIgnoreCase("N"))
                                    adaIB = true;
                            }
                        }
                        if(!paymentType.equals("")) {
                            JSONArray arrPaymentType = new JSONArray(paymentType);
                            PaymentTypeList.add(new PaymentTypeBean("0", getResources().getString(R.string.pilih_tipe_pembayaran)));
                            for (int i = 0; i < arrPaymentType.length(); i++) {
                                if(arrPaymentType.getJSONObject(i).getString(AppParams.PAYMENT_CODE).equalsIgnoreCase(AppParams.ESPAY)) {
                                    if(adaIB) PaymentTypeList.add(new PaymentTypeBean(AppParams.ESPAYIB, "Espay Online Payment"));
                                }
                                else
                                   PaymentTypeList.add(new PaymentTypeBean(arrPaymentType.getJSONObject(i).getString(AppParams.PAYMENT_CODE), arrPaymentType.getJSONObject(i).getString(AppParams.PAYMENT_NAME)));
                            }
                            PaymentTypeList.add(new PaymentTypeBean(AppParams.ESPAYSMS, "Espay SMS Payment"));

                        }
                        else {
                            PaymentTypeList.add(new PaymentTypeBean("0", getResources().getString(R.string.tidak_ada_data)));
                        }


                        PaymentTypeAdapter paymentTypeAdapter = new PaymentTypeAdapter(activity, android.R.layout.simple_spinner_item, PaymentTypeList){
                            @Override
                            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                                View v = null;

                                // If this is the initial dummy entry, make it hidden
                                if (position == 0) {
                                    TextView tv = new TextView(getContext());
                                    tv.setHeight(0);
                                    tv.setVisibility(View.GONE);
                                    v = tv;
                                }
                                else {
                                    // Pass convertView as null to prevent reuse of special case views
                                    v = super.getDropDownView(position, null, parent);
                                }

                                // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
                                parent.setVerticalScrollBarEnabled(false);
                                return v;
                            }
                        };

                        cbo_payment_type.setAdapter(paymentTypeAdapter);
                        cbo_payment_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                if(position == 0) {

                                }
                                else {
                                    if(alertShow) return;;
                                    String remarkHint = null;
                                    String type = null;

                                    type = PaymentTypeList.get(position).getPayment_type();

                                    if (PaymentTypeList.get(position).getPayment_type().equalsIgnoreCase(AppParams.BG))
                                        remarkHint = "BG ID";
                                    else if (PaymentTypeList.get(position).getPayment_type().equalsIgnoreCase(AppParams.TS))
                                        remarkHint = "Account No";
                                    else remarkHint = "Payment Remark";

                                    if (PaymentTypeList.get(position).getPayment_type().equalsIgnoreCase(payment_type_code)) {
                                        alertShow = true;
                                        FragmentManager fragmentManager = getFragmentManager();
                                        dialog = new DialogFragmentDGISales();
                                        Bundle args = new Bundle();
                                        args.putString("remark_hint", remarkHint);
                                        args.putString("payment_type", type);
                                        args.putString("payment_bank_code", payment_bankcode);
                                        args.putString("payment_remark", payment_remark);
                                        args.putString("payment_product_code", payment_productcode);
                                        dialog.setArguments(args);
                                        dialog.setCancelable(false);
                                        dialog.setTargetFragment(FragmentInvoiceListDGI.this, 100);
                                        dialog.show(fragmentManager, "dialogDGI");
                                    }
                                    else {
                                        alertShow = true;
                                        FragmentManager fragmentManager = getFragmentManager();
                                        dialog = new DialogFragmentDGISales();
                                        Bundle args = new Bundle();
                                        args.putString("remark_hint", remarkHint);
                                        args.putString("payment_type", type);
                                        args.putString("payment_bank_code", "0");
                                        args.putString("payment_remark", "");
                                        args.putString("payment_product_code", "0");
                                        dialog.setArguments(args);
                                        dialog.setCancelable(false);
                                        dialog.setTargetFragment(FragmentInvoiceListDGI.this, 100);
                                        dialog.show(fragmentManager, "dialogDGI");
                                    }

                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                        phone_type = json.getString(AppParams.PHONE_TYPE);

                        if(phone_type.equalsIgnoreCase("M")) {
                            row_phone.setVisibility(View.VISIBLE);
                            if (!json.getString(AppParams.PHONE_DATA).equals("")) {
                                JSONArray phoneData = new JSONArray(json.getString(AppParams.PHONE_DATA));
                                listPhone.add(new PaymentTypeBean("0", getResources().getString(R.string.pilih_nomor_hp)));
                                for (int i = 0; i < phoneData.length(); i++) {
                                    listPhone.add(new PaymentTypeBean(Integer.toString(i + 1), phoneData.getJSONObject(i).getString(AppParams.MOBILE_PHONE)));
                                }
                            } else {
                                listPhone.add(new PaymentTypeBean("0", getResources().getString(R.string.tidak_ada_data)));
                            }

                            PaymentTypeAdapter phoneAdapter = new PaymentTypeAdapter(activity, android.R.layout.simple_spinner_item, listPhone) {
                                @Override
                                public View getDropDownView(int position, View convertView, ViewGroup parent) {
                                    View v = null;

                                    // If this is the initial dummy entry, make it hidden
                                    if (position == 0) {
                                        TextView tv = new TextView(getContext());
                                        tv.setHeight(0);
                                        tv.setVisibility(View.GONE);
                                        v = tv;
                                    } else {
                                        // Pass convertView as null to prevent reuse of special case views
                                        v = super.getDropDownView(position, null, parent);
                                    }

                                    // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
                                    parent.setVerticalScrollBarEnabled(false);
                                    return v;
                                }
                            };

                            cbo_phone_number.setAdapter(phoneAdapter);
                            cbo_phone_number.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    if (position == 0) {

                                    } else {
                                        phone_number = listPhone.get(position).getPayment_name();
                                    }
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });
                        }
                        else {
                            row_phone.setVisibility(View.GONE);
//                            phone_number = sp.getString("user_phone","");
                            phone_number = "";
                        }

                        JSONArray data = json.getJSONArray("invoice_data"); // this is the "items: [ ] part

                        partial_payment = json.getString("partial_payment");
                        total_pay       = json.getString("total_pay");
                        session_id_var  = json.getString("session_id_var");

                        JSONArray dataFee    = json.getJSONArray("fee_data");
                        JSONObject objectFee = dataFee.getJSONObject(0);
                        ccy_id               =  objectFee.getString("ccy_id");
                        buyer_fee            =  objectFee.getString("buyer_fee");
                        commission_fee       =  objectFee.getString("commission_fee");
                        seller_fee           =  objectFee.getString("seller_fee");
                        min_amount           =  objectFee.getString("min_amount");
                        max_amount           =  objectFee.getString("max_amount");

                        String total_payment = (total_pay != null && !total_pay.equals("") && !total_pay.equals("null") && Double.parseDouble(total_pay) > 0 )  ? FormatCurrency.getRupiahFormat(total_pay) : "";
                        lbl_total_pay_amount.setText(total_payment);

                        for (int i = 0; i < data.length(); i++) {
                            JSONObject object = data.getJSONObject(i);
                            doc_no.add(object.getString("doc_no"));
                            doc_id.add(object.getString("doc_id"));
                            amount.add(object.getString("amount"));
                            remain_amount.add(object.getString("remain_amount"));
                            hold_amount.add(object.getString("hold_amount"));
                            ccy.add(object.getString("ccy"));
                            doc_desc.add(object.getString("doc_desc"));
                            due_date.add(object.getString("due_date"));
                            input_amount.add(object.getString("input_amount"));
                            session_id.add(object.getString("session_id"));
                        }

                        // when finish parsing, hide progressbar
                        prgLoading.setVisibility(View.GONE);

                        // if data available show data on list
                        // otherwise, show alert text
                        if(doc_no.size() > 0){
                            listMenu.setVisibility(View.VISIBLE);
                            listMenu.setAdapter(invoiceDGIAdapter);
                            lbl_header.setVisibility(View.VISIBLE);
                            tabel_footer.setVisibility(View.VISIBLE);
                        }else{
                            txtAlert.setVisibility(View.VISIBLE);
                        }
                    }else{
                        prgLoading.setVisibility(View.GONE);
                        txtAlert.setVisibility(View.VISIBLE);
                        txtAlert.setText(error_message);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                }
            }

            public void onFailure(Throwable error, String content) {
                Toast.makeText(activity, "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
            }
        });
    }

    // clear arraylist variables before used
    void clearData(){
        doc_no.clear();
        doc_id.clear();
        amount.clear();
        remain_amount.clear();
        hold_amount.clear();
        ccy.clear();
        doc_desc.clear();
        due_date.clear();
        input_amount.clear();
        session_id.clear();
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }

    @Override
    public void onFinishDialog(){
        PaymentTypeBean paymentSelected  = (PaymentTypeBean) cbo_payment_type.getSelectedItem();
        payment_type_code   = String.valueOf(paymentSelected.payment_type);
        payment_type_name   = String.valueOf(paymentSelected.payment_name);
        alertShow = false;

        if(phone_type.equalsIgnoreCase("M")) {
            if (payment_type_code.equalsIgnoreCase(AppParams.ESPAYSMS) || payment_type_code.equalsIgnoreCase(AppParams.ESPAYIB)) {
                //hide phone number
                row_phone.setVisibility(View.GONE);
            } else row_phone.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onFinishEditDialog(String inputText) {
        payment_remark = inputText;
        alertShow = false;


    }

    @Override
    public void onFinishSpinnerBankDialog(String bankCode, String bankName) {
        payment_bankcode = bankCode;
        payment_bankname = bankName;
        alertShow = false;
    }

    @Override
    public void onFinishSpinnerProductDialog(String productCode, String productName, String product_h2h) {
        if(payment_type_code.equalsIgnoreCase(AppParams.ESPAYSMS) || payment_type_code.equalsIgnoreCase(AppParams.ESPAYIB)) {
            alertShow = false;
            payment_productname = productName;
            payment_product_h2h = product_h2h;
            if(payment_product_h2h.equalsIgnoreCase("Y")) {
                payment_bankcode = "008";
                payment_productcode = "MANDIRISMS";
                isSMSBanking = true;
                product_values = AppHelper.getUserPhone(activity);
            }else {
                payment_productcode = productCode;
                isSMSBanking = false;
            }
        }
        else {
            payment_productcode = productCode;
            payment_productname = productName;
            payment_product_h2h = product_h2h;
            alertShow = false;
            if (payment_product_h2h.equalsIgnoreCase("Y")) {
                isSMSBanking = true;
                product_values = AppHelper.getUserPhone(activity);
            } else
                isSMSBanking = false;
        }

    }

    @Override
    public void onDismiss(boolean dismiss) {
        if(dismiss) {
            cbo_payment_type.setSelection(0);
            payment_remark = "";
            payment_bankcode = "";
            payment_bankname = "";
            alertShow = false;
        }
    }

    private void clearPaymentData(){
        payment_type_code = "";
        dialog.spinnerProduct.setSelection(0);
        cbo_payment_type.setSelection(0);
    }

}