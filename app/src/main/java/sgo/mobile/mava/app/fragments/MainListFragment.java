package sgo.mobile.mava.app.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import sgo.mobile.mava.AppHelper;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.activities.MainActivity;

public class MainListFragment extends Fragment {
    OnFragmentUpdateListener mCallback;
    private Context context;

    public interface OnFragmentUpdateListener {
        public void onFragmentUpdate(int position, boolean forceupdate);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mCallback = (OnFragmentUpdateListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentUpdateListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(container != null) {
            container.removeAllViews();
        }

        context = getActivity().getApplicationContext();

        View view = inflater.inflate(R.layout.viewpager_main, container, false);
        createPagerView(view);
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    public void createPagerView(View view) {
        ViewPager awesomePager = (ViewPager) view.findViewById(R.id.pager);
        awesomePager.setAdapter(new SgoPagerAdapter());

//        TabPageIndicator titleIndicator = (TabPageIndicator)view.findViewById(R.id.titles);
//        titleIndicator.setViewPager(awesomePager);
    }

    public static MainListFragment newInstance() {
        MainListFragment f = new MainListFragment();

        Bundle args = new Bundle();
        f.setArguments(args);

        return f;
    }

    private class SgoPagerAdapter extends PagerAdapter {
        String[] pages = {" "};
        String pageContent;

        private SgoPagerAdapter() {

        }

        @Override
        public int getCount() {
            return pages.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return (view == object);
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object view) {
            ((ViewPager) container).removeView((View) view);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View parent_view = null;
            if (position ==0) {
                parent_view = getViewForPageOne();
                ((ViewPager) container).addView(parent_view, 0);
            }else if (position == 1){
                parent_view = getViewForPageTwo();
                ((ViewPager) container).addView(parent_view, 0);
            }else if (position == 2){
                parent_view = getViewForPageThree();
                ((ViewPager) container).addView(parent_view, 0);
            }else if (position == 3){
                parent_view = getViewForPageFour();
                ((ViewPager) container).addView(parent_view, 0);
            }else if (position == 4){
                parent_view = getViewForPageFive();
                ((ViewPager) container).addView(parent_view, 0);
            }else if (position == 5){
                parent_view = getViewForPageSix();
                ((ViewPager) container).addView(parent_view, 0);
            }
            return parent_view;
        }

        private View getViewForPageOne(){

            String user_id = AppHelper.getUserPhone(getActivity());
            LayoutInflater inflater = (LayoutInflater)context.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.view_no_transaksi, null);
            TextView txtSaldo =(TextView) view.findViewById(R.id.txtSaldo);
            txtSaldo.setText(getResources().getString(R.string.title_welcome) + " " + user_id);

            return view;
        }

        private View getViewForPageTwo(){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.view_no_transaksi, null);
            TextView txtSaldo =(TextView) view.findViewById(R.id.txtSaldo);
            txtSaldo.setText("Rp.200.000,00");

            return view;
        }

        private View getViewForPageThree(){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.view_no_transaksi, null);
            TextView txtSaldo =(TextView) view.findViewById(R.id.txtSaldo);
            txtSaldo.setText("Rp.250.000,00");

            return view;
        }

        private View getViewForPageFour(){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.view_no_transaksi, null);
            TextView txtSaldo =(TextView) view.findViewById(R.id.txtSaldo);
            txtSaldo.setText("Rp.700.000,00");

            return view;
        }

        private View getViewForPageFive(){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.view_no_transaksi, null);
            TextView txtSaldo =(TextView) view.findViewById(R.id.txtSaldo);
            txtSaldo.setText("Rp.50.000,00");

            return view;
        }

        private View getViewForPageSix(){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.view_no_transaksi, null);
            TextView txtSaldo =(TextView) view.findViewById(R.id.txtSaldo);
            txtSaldo.setText("Rp.70.000,00");

            return view;
        }

        public String getPageTitle(int position) {
            return pages[position];
        }

        private void switchFragment(Fragment fragment) {
            if (getActivity() == null)
                return;
            MainActivity main = (MainActivity) getActivity();
            main.switchContent(fragment);
        }
    }
}
