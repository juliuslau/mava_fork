package sgo.mobile.mava.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.beans.InvoicePayPaymentBean;
import sgo.mobile.mava.app.fragments.FragmentInvoicePayPayment;
import sgo.mobile.mava.frameworks.math.FormatCurrency;

import java.util.ArrayList;

public class InvoicePayPaymentAdapter extends BaseAdapter {
    private ArrayList<InvoicePayPaymentBean> data;
    private Context context;

    public InvoicePayPaymentAdapter(Context context, ArrayList<InvoicePayPaymentBean> data) {
        this.context = context;
        this.data = data;
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.fragment_invdgipay_list_item, null);
            holder = new ViewHolder();

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        String InputAmount = data.get(position).getInput_amount_pay();
        int remain_amount = Integer.parseInt(data.get(position).getRemain_amount_pay());
        holder.txtText    = (TextView) convertView.findViewById(R.id.txtText);
        holder.txtSubText = (TextView) convertView.findViewById(R.id.txtSubText);
        holder.txtSubText2 = (TextView) convertView.findViewById(R.id.txtSubText2);

        holder.txtText.setText("Invoice " + data.get(position).getDoc_no_pay());
        holder.txtSubText.setText( "Sisa : " +  FormatCurrency.getRupiahFormat(Integer.toString(remain_amount)));

        if (InputAmount != null && !InputAmount.equals("") && !InputAmount.equals("null") && Integer.parseInt(InputAmount) > 0){
            String buyer_fee      = FragmentInvoicePayPayment.buyer_fee;
            buyer_fee             = (buyer_fee != null && !buyer_fee.equals("") && !buyer_fee.equals("null") && Integer.parseInt(buyer_fee) > 0)? buyer_fee : "0";
            int total_pay_plus_fee     = Integer.parseInt(InputAmount) + Integer.parseInt(buyer_fee);
            holder.txtSubText2.setVisibility(View.VISIBLE);
            holder.txtSubText2.setText( "Bayar : " +  FormatCurrency.getRupiahFormat(Integer.toString(total_pay_plus_fee)) + " (Fee : " + FormatCurrency.getRupiahFormat(buyer_fee) + ")");
        }else{
            holder.txtSubText2.setVisibility(View.GONE);
        }

        return convertView;
    }

    static class ViewHolder {
        TextView txtText, txtSubText, txtSubText2;
    }
}
