package sgo.mobile.mava.app.beans;

/**
 * Created by thinkpad on 1/20/2016.
 */
public class BankKMKBean {

    private String bank_code;
    private String bank_name;
    private String product_code;
    private String product_name;
    private String tx_fee;
    private String buyer_fee;
    private String seller_fee;
    private String product_h2h;

    public String getBuyer_fee() {
        return buyer_fee;
    }

    public void setBuyer_fee(String buyer_fee) {
        this.buyer_fee = buyer_fee;
    }

    public String getProduct_h2h() {
        return product_h2h;
    }

    public void setProduct_h2h(String product_h2h) {
        this.product_h2h = product_h2h;
    }

    public String getSeller_fee() {
        return seller_fee;
    }

    public void setSeller_fee(String seller_fee) {
        this.seller_fee = seller_fee;
    }

    public String getTx_fee() {
        return tx_fee;
    }

    public void setTx_fee(String tx_fee) {
        this.tx_fee = tx_fee;
    }

    public String getBank_code() {
        return bank_code;
    }

    public void setBank_code(String bank_code) {
        this.bank_code = bank_code;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }
}
