package sgo.mobile.mava.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.beans.ReportCommunityBean;
import sgo.mobile.mava.frameworks.math.FormatCurrency;

import java.util.ArrayList;

public class CommunityDGIReportAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<ReportCommunityBean> arrReportComm;

    public CommunityDGIReportAdapter(Activity act, ArrayList<ReportCommunityBean> arrReportComm) {
        this.activity = act;
        this.arrReportComm = arrReportComm;
    }
    public int getCount() {
        // TODO Auto-generated method stub
        return arrReportComm.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_comm_dgi_report_item, null);
            holder = new ViewHolder();

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        Double dbl_amount = Double.parseDouble(arrReportComm.get(position).getAmount());
        int int_amount = dbl_amount.intValue();

        holder.txtText    = (TextView) convertView.findViewById(R.id.txtText);
        holder.txtSubText1 = (TextView) convertView.findViewById(R.id.txtSubText1);
        holder.txtSubText2 = (TextView) convertView.findViewById(R.id.txtSubText2);

        holder.txtText.setText(arrReportComm.get(position).getComm_name());
        holder.txtSubText1.setText("Sales Alias : " + arrReportComm.get(position).getSales_alias());
        holder.txtSubText2.setText("Jumlah : " +  FormatCurrency.getRupiahFormat(Integer.toString(int_amount)));

        return convertView;
    }

    static class ViewHolder {
        TextView txtText, txtSubText1, txtSubText2;
    }
}
