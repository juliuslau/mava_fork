package sgo.mobile.mava.app.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;
import in.srain.cube.views.ptr.header.StoreHouseHeader;
import sgo.mobile.mava.conf.AplConstants;
import sgo.mobile.mava.conf.AppParams;
import sgo.mobile.mava.frameworks.net.loopj.android.http.MySSLSocketFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.mava.AppHelper;
import sgo.mobile.mava.R;
import sgo.mobile.mava.app.activities.MainActivity;
import sgo.mobile.mava.app.adapter.DetailDGIReportAdapter;
import sgo.mobile.mava.frameworks.math.FormatCurrency;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.mava.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.mava.frameworks.net.loopj.android.http.RequestParams;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class FragmentDetailReportDGI extends Fragment {
    FragmentManager fm;

    public String comm_id, comm_name, comm_code, sales_alias, comm_ccy, comm_amount, member_id, amount_master, date_from, date_to, type;
    public static String member_shop, ccy_id, amount_detail, fee;

    // declare view objects
    ListView listMenu;
    ProgressBar prgLoading;
    TextView txtAlert;
    TextView lbl_header, lbl_jumlah, lbl_fee;
    TableLayout tabel_header;
    TableLayout tabel_footer;

    DetailDGIReportAdapter detailDGIAdapter;

    // create arraylist variables to store data from server
    public static ArrayList<String> doc_no = new ArrayList<String>();
    public static ArrayList<String> amount = new ArrayList<String>();
    public static ArrayList<String> payment_type = new ArrayList<String>();

    // create price format
    DecimalFormat formatData = new DecimalFormat("#.##");

    Button btnBack;

    int page = 0;
    private PtrFrameLayout mPtrFrame;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_detail_dgi_report, container, false);
        txtAlert = (TextView) view.findViewById(R.id.txtAlert);
        prgLoading = (ProgressBar) view.findViewById(R.id.prgLoading);
        mPtrFrame = (PtrFrameLayout) view.findViewById(R.id.report_ptr_frame);
        listMenu   = (ListView) view.findViewById(R.id.listMenu);
        lbl_header = (TextView) view.findViewById(R.id.label_header);
        tabel_header = (TableLayout) view.findViewById(R.id.tabel_header);
        tabel_footer = (TableLayout) view.findViewById(R.id.tabel_footer);

        Bundle bundle         = this.getArguments();
        comm_id               = bundle.getString(AppParams.COMM_ID);
        comm_name             = bundle.getString(AppParams.COMM_NAME);
        comm_code             = bundle.getString(AppParams.COMM_CODE);
        sales_alias           = bundle.getString(AppParams.SALES_ALIAS);
        comm_ccy              = bundle.getString(AppParams.COMM_CCY);
        comm_amount           = bundle.getString(AppParams.COMM_AMOUNT);

        member_id             = bundle.getString(AppParams.MEMBER_ID);
        member_shop           = bundle.getString(AppParams.MEMBER_SHOP);
        ccy_id                = bundle.getString(AppParams.CCY_ID);
        amount_master         = bundle.getString(AppParams.AMOUNT);
        date_from             = bundle.getString(AppParams.DATE_FROM);
        date_to               = bundle.getString(AppParams.DATE_TO);
        type                  = bundle.getString(AppParams.PAYMENT_TYPE);

        detailDGIAdapter = new DetailDGIReportAdapter(getActivity());
        parseJSONData();

        lbl_header = (TextView) view.findViewById(R.id.label_header);
        lbl_header.setText(member_shop);

        Double dbl_amount = Double.parseDouble(amount_master);
        int int_amount = dbl_amount.intValue();

        lbl_jumlah = (TextView) view.findViewById(R.id.lbl_jumlah);
        lbl_jumlah.setText(FormatCurrency.getRupiahFormat(Integer.toString(int_amount)));

        StoreHouseHeader header = new StoreHouseHeader(getActivity().getApplicationContext());
        header.setPadding(0, 20, 0, 20);
        header.setTextColor(Color.BLACK);
        header.initWithString("Updating...");

        mPtrFrame.setDurationToCloseHeader(1500);
        mPtrFrame.setHeaderView(header);
        mPtrFrame.addPtrUIHandler(header);
        mPtrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                frame.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(txtAlert.getVisibility() == View.VISIBLE) {

                        }
                        else {
                            page++;
                            parseJSONData();
                        }
                        mPtrFrame.refreshComplete();
                    }
                }, 1800);
            }

            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                return !canScrollUp(((ListView) content)); // or cast with ListView
            }

            public boolean canScrollUp(View view) {
                if (android.os.Build.VERSION.SDK_INT < 14) {
                    if (view instanceof AbsListView) {
                        final AbsListView absListView = (AbsListView) view;
                        return absListView.getChildCount() > 0
                                && (absListView.getFirstVisiblePosition() > 0 || absListView
                                .getChildAt(0).getTop() < absListView.getPaddingTop());
                    } else {
                        return view.getScrollY() > 0;
                    }
                } else {
                    return ViewCompat.canScrollVertically(view, -1);
                }
            }
        });

        btnBack             = (Button) view.findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                clearData();
                Fragment newFragment = new FragmentMemberReportDGI();
                Bundle args = new Bundle();
                args.putString(AppParams.COMM_ID, comm_id);
                args.putString(AppParams.COMM_NAME, comm_name);
                args.putString(AppParams.COMM_CODE, comm_code);
                args.putString(AppParams.SALES_ALIAS, sales_alias);
                args.putString(AppParams.CCY_ID, comm_ccy);
                args.putString(AppParams.AMOUNT, comm_amount);
                args.putString(AppParams.DATE_FROM, date_from);
                args.putString(AppParams.DATE_TO, date_to);
                args.putString(AppParams.PAYMENT_TYPE, type);

                newFragment.setArguments(args);
                getActivity().getSupportFragmentManager().popBackStack();
                switchFragment(newFragment);
            }
        });

        return view;
    }

    public void parseJSONData(){
        try {
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
            RequestParams params = new RequestParams();

            String param_sales_id = AppHelper.getUserId(getActivity());

            params.put(AppParams.COMM_ID, comm_id);
            params.put(AppParams.MEMBER_ID, member_id);
            params.put(AppParams.SALES_ID, param_sales_id);
            params.put(AppParams.CCY_ID, ccy_id);
            params.put(AppParams.PAGE, Integer.toString(page));
            params.put(AppParams.DATE_FROM, date_from);
            params.put(AppParams.DATE_TO, date_to);
            params.put(AppParams.PAYMENT_TYPE, type);

            Log.d("params", params.toString());
            client.post(AplConstants.DiReportDetail, params, new AsyncHttpResponseHandler() {
                public void onSuccess(String content) {
                    Log.d("result:", content);
                    try {
                        // parse json data and store into arraylist variables
                        JSONObject json = new JSONObject(content);
                        String error_code = json.getString("error_code");
                        String error_message = json.getString("error_message");

                        if (error_code.equals(AppParams.SUCCESS_CODE)) {

                            member_shop = json.getString("member_shop");
                            ccy_id = json.getString("ccy_id");
                            amount_detail = json.getString("amount");
                            fee = json.getString("fee");

                            JSONArray data = json.getJSONArray("report_data"); // this is the "items: [ ] part
                            for (int i = 0; i < data.length(); i++) {
                                String id = data.getJSONObject(i).getString(AppParams.DOC_NO);
                                boolean flagSame = false;

                                // cek apakah ada member id yang sama.. kalau ada, tidak dimasukan ke array
                                if (doc_no.size() > 0) {
                                    for (int index = 0; index < doc_no.size(); index++) {
                                        if (doc_no.get(index).equalsIgnoreCase(id)) {
                                            flagSame = true;
                                            break;
                                        } else {
                                            flagSame = false;
                                        }
                                    }
                                }

                                if (flagSame == false) {
                                    JSONObject object = data.getJSONObject(i);
                                    doc_no.add(object.getString(AppParams.DOC_NO));
                                    amount.add(object.getString(AppParams.AMOUNT));
                                    payment_type.add(object.getString(AppParams.PAYMENT_TYPE));
                                }
                            }

                            // when finish parsing, hide progressbar
                            prgLoading.setVisibility(View.GONE);

                            // if data available show data on list
                            // otherwise, show alert text
                            if (doc_no.size() > 0) {
                                listMenu.setVisibility(View.VISIBLE);
                                listMenu.setAdapter(detailDGIAdapter);
                                lbl_header.setVisibility(View.VISIBLE);
                                tabel_header.setVisibility(View.VISIBLE);
                                tabel_footer.setVisibility(View.VISIBLE);

                                Double dbl_fee = Double.parseDouble(fee);
                                int int_fee = dbl_fee.intValue();

                                lbl_fee = (TextView) getActivity().findViewById(R.id.lbl_fee);
                                lbl_fee.setText(FormatCurrency.getRupiahFormat(Integer.toString(int_fee)));

                            } else {
                                txtAlert.setVisibility(View.VISIBLE);
                            }

                        } else if (doc_no.size() == 0) {
                            prgLoading.setVisibility(View.GONE);
                            txtAlert.setVisibility(View.VISIBLE);
                            txtAlert.setText(error_message);
                        }

                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                };

                public void onFailure(Throwable error, String content) {
                    prgLoading.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                };
            });
        }

        catch (Exception e){
        }
    }

    // clear arraylist variables before used
    void clearData(){
        doc_no.clear();
        amount.clear();
        payment_type.clear();
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }

}