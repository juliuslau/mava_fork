package sgo.mobile.mava.frameworks.security;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Sha1 {

    /**
     * Create a SHA-1 digest of the provided message.
     *
     * @param message The message to digest.
     *
     * @return The digest of the message using SHA-1.
     */
    public static String hashSHA256(String input)
            throws NoSuchAlgorithmException {
        MessageDigest mDigest = MessageDigest.getInstance("SHA-256");

        byte[] shaByteArr = mDigest.digest(input.getBytes(Charset.forName("UTF-8")));
        StringBuilder hexStrBuilder = new StringBuilder();
        for (int i = 0; i < shaByteArr.length; i++) {
            hexStrBuilder.append(String.format("%02x", shaByteArr[i]));
        }

        return hexStrBuilder.toString();
    }
}
