package sgo.mobile.mava.frameworks.security;

import javax.crypto.*;
import javax.crypto.spec.DESKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

public class DES implements IEncrypt , IDecrypt{

    /**
     * @param args
     */
    String part1 = "486135738435";
    String part2 = "337846125153";
    public String encrypt(String str){
        SecureRandom sr = new SecureRandom();
        byte rawKeyData[] = (part1.substring(3, 8) + part2.substring(4,10)).getBytes();
        byte[] encryptedData = new byte[0];
        String result = "";
        try {
            DESKeySpec dks = new DESKeySpec(rawKeyData);
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey key = keyFactory.generateSecret(dks);
            Cipher cipher = Cipher.getInstance("DES");
            cipher.init(Cipher.ENCRYPT_MODE, key, sr);
            byte data[] = str.getBytes("ISO-8859-1");
            encryptedData = cipher.doFinal(data);
            result = new String(encryptedData , "ISO-8859-1");
        } catch (InvalidKeyException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }catch (InvalidKeySpecException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }catch (NoSuchPaddingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }catch (IllegalBlockSizeException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (BadPaddingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return result;
    }
    public String decrypt( String encryptedData){
        SecureRandom sr = new SecureRandom();
        byte rawKeyData[] = (part1.substring(3, 8) + part2.substring(4,10)).getBytes();
        byte decryptedData[] = new byte[0];
        String result = "";
        try {
            DESKeySpec dks = new DESKeySpec(rawKeyData);
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey key = keyFactory.generateSecret(dks);
            Cipher cipher = Cipher.getInstance("DES");
            cipher.init(Cipher.DECRYPT_MODE, key, sr);
            decryptedData = cipher.doFinal(encryptedData.getBytes("ISO-8859-1"));
            result = new String(decryptedData , "ISO-8859-1");
        } catch (InvalidKeyException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }catch (InvalidKeySpecException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }catch (NoSuchPaddingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }catch (IllegalBlockSizeException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (BadPaddingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;
    }

    public static void main(String[] args) throws NoSuchAlgorithmException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchPaddingException, InvalidKeySpecException {
        DES ed = new DES();
        String str = "mobile.sgo.co.id/api";
        String encryptedData = ed.encrypt(str);
        System.out.println("Encrypted string ===>" + encryptedData);
        System.out.println("Decrypted string ===>" + ed.decrypt(encryptedData));
    }
}
