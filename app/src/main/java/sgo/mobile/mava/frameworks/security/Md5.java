package sgo.mobile.mava.frameworks.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Md5{
    public static final String hashMd5(String p) throws NoSuchAlgorithmException{

        MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
        digest.update(p.getBytes());
        byte messageDigest[] = digest.digest();

        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < messageDigest.length; i++) {
            String h = Integer.toHexString(0xFF & messageDigest[i]);
            while (h.length() < 2)
                h = "0" + h;
            hexString.append(h);
        }
        return hexString.toString();
    }
}