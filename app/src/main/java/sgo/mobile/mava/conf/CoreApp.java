package sgo.mobile.mava.conf;

import android.app.Application;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;

import sgo.mobile.mava.app.beans.InvoiceRepaymentBean;

/**
 * Created by thinkpad on 1/19/2016.
 */
public class CoreApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        Configuration.Builder configurationBuilder = new Configuration.Builder(getApplicationContext());
        configurationBuilder.addModelClasses(
                InvoiceRepaymentBean.class
        );
        ActiveAndroid.initialize(configurationBuilder.create());
    }
}
